import 'package:attendanceapp/blocs/authentication/blocs.dart';
import 'package:bloc/bloc.dart';
import 'package:attendanceapp/repositories/user/repositories.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class AuthenticationBloc extends Bloc<AuthenticationEvent,AuthenticationState>{
  final UserRepository userRepository;
  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null), userRepository = userRepository;

  @override
  AuthenticationState get initialState => Uninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async*{

    if(event is AppStarted){
     // await DatabaseHelper.get().init();
      yield* _mapAppStartedToState();
    }else if(event is Logout){
      yield Unauthenticated();
    }

  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = (prefs.getString('token') ?? "");
   // User user = await DatabaseHelper.get().getActiveUser();

    if(token != ""){
     yield Authenticated();
    }else{
      yield Unauthenticated();
    }
  }
}