import 'package:attendanceapp/models/attendance_info.dart';
import 'package:equatable/equatable.dart';

abstract class TeacherAttendanceState extends Equatable{
  const TeacherAttendanceState();

  @override
  List<Object> get props => [];
}

class TeacherAttendanceUninitialized extends TeacherAttendanceState{}

class TeacherAttendanceLoading extends TeacherAttendanceState{}
class TeacherAttendanceLoaded extends TeacherAttendanceState{
  final AttendanceInfo attendanceInfo;
  TeacherAttendanceLoaded({this.attendanceInfo});

  @override
  List<Object> get props => [attendanceInfo];
}
class TeacherAttendanceSuccess extends TeacherAttendanceState{}
class TeacherAttendanceError extends TeacherAttendanceState{
  final String msg;
  TeacherAttendanceError({this.msg});

  @override
  List<Object> get props => [msg];
}

