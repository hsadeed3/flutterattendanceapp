import 'package:equatable/equatable.dart';
import 'dart:io';
abstract class TeacherAttendanceEvent extends Equatable{
  const TeacherAttendanceEvent();

  @override
  List<Object> get props => [];
}

class TeacherAttendanceInitialized extends TeacherAttendanceEvent{
  final int teacherId;
  TeacherAttendanceInitialized({this.teacherId});
  @override
  List<Object> get props => [teacherId];
}

class SendTeacherAttendance extends TeacherAttendanceEvent{
  final int attendanceId;
  final String checkOutTime;
  final File photoCheckOut;
  SendTeacherAttendance({this.attendanceId,this.checkOutTime,this.photoCheckOut});

  @override
  List<Object> get props => [attendanceId,checkOutTime,photoCheckOut];
}

  class CreateTeacherAttendance extends TeacherAttendanceEvent{
  final String teacherId;
  final String checkInTime;
  final String temperature;
  final String date;
  final File checkInPic;
  final String notes;
  CreateTeacherAttendance({this.teacherId,this.checkInTime,this.temperature,this.date, this.checkInPic,this.notes});

  @override
  List<Object> get props => [teacherId,checkInTime,temperature,notes,date,checkInPic,notes];
}