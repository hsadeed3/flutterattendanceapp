import 'package:attendanceapp/blocs/teacher_attendance/blocs.dart';
import 'package:attendanceapp/blocs/teacher_attendance/teacher_attendance_event.dart';
import 'package:attendanceapp/models/attendance_info.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:attendanceapp/models/state.dart';
import 'package:attendanceapp/repositories/teacher_attendance/repositories.dart';

class TeacherAttendanceBloc extends Bloc<TeacherAttendanceEvent, TeacherAttendanceState>{

  final TeacherAttendanceRepository teacherAttendanceRepository;
  TeacherAttendanceBloc({@required this.teacherAttendanceRepository})
      : assert(teacherAttendanceRepository != null);


  @override
  Stream<TeacherAttendanceState> mapEventToState(TeacherAttendanceEvent event) async* {
    if (event is TeacherAttendanceInitialized){

      yield TeacherAttendanceLoading();
      try {
        final AttendanceInfo attendanceInfo = await teacherAttendanceRepository.getTeacherAttendanceInfo(event.teacherId);
        yield TeacherAttendanceLoaded(attendanceInfo: attendanceInfo);
      }catch (_) {
        yield TeacherAttendanceError();
      }

    }
    if (event is CreateTeacherAttendance) {
      yield TeacherAttendanceLoading();
      final StateModel attendanceInfoModel = await this
          .teacherAttendanceRepository.createNewAttendance(
          event.teacherId, event.checkInTime,  event.temperature,event.date, event.checkInPic, event.notes);
      if (attendanceInfoModel is SuccessState) {
        yield TeacherAttendanceSuccess();
      }
      else if (attendanceInfoModel is ErrorState) {
        yield TeacherAttendanceError(msg: attendanceInfoModel.msg);
      }
    }
    if (event is SendTeacherAttendance){
      yield TeacherAttendanceLoading();
      final StateModel attendanceInfoModel = await this.teacherAttendanceRepository.sendAttendanceInfo(event.attendanceId,event.checkOutTime,event.photoCheckOut);
      if (attendanceInfoModel is SuccessState){
        yield TeacherAttendanceSuccess();
      }
      else if (attendanceInfoModel is ErrorState){
        yield TeacherAttendanceError(msg: attendanceInfoModel.msg);
      }
    }
  }

  @override
  TeacherAttendanceState get initialState => TeacherAttendanceUninitialized();

}