import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class TeacherEvent extends Equatable {
  const TeacherEvent();
}

class FetchTeacher extends TeacherEvent {
  final int instituteId;

  const FetchTeacher({@required this.instituteId}) : assert(instituteId!=null);

  @override
  List<Object> get props => [instituteId];

}