import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:attendanceapp/models/teacher.dart';
import 'package:attendanceapp/repositories/teacher/repositories.dart';
import 'package:attendanceapp/blocs/teacher/blocs.dart';


class TeacherBloc extends Bloc<TeacherEvent,TeacherState>{
  final TeacherRepository teacherRepository;

  TeacherBloc({@required this.teacherRepository})
      : assert(teacherRepository != null);

  @override
  TeacherState get initialState => TeacherEmpty();

  @override
  Stream<TeacherState> mapEventToState(TeacherEvent event) async* {
    if (event is FetchTeacher) {
      yield TeacherLoading();
      try {
        final List<Teacher> teachers = await teacherRepository.getTeachersByInstitute(event.instituteId);
        yield TeacherLoaded(teachers: teachers);
      } catch (_) {
        yield TeacherError();
      }
    }
  }
}