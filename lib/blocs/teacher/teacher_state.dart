import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:attendanceapp/models/teacher.dart';

abstract class TeacherState extends Equatable {
  const TeacherState();
  @override
  List<Object> get props => [];
}

class TeacherEmpty extends TeacherState {}

class TeacherLoading extends TeacherState {}

class TeacherLoaded extends TeacherState {
  final List<Teacher> teachers;

  const TeacherLoaded({@required this.teachers}) : assert(teachers != null);

  @override
  List<Object> get props => [teachers];
}
class TeacherError extends TeacherState {}