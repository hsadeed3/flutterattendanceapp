import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:attendanceapp/blocs/user/user_event.dart';
import 'package:attendanceapp/blocs/user/user_state.dart';
import 'package:attendanceapp/repositories/user/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:attendanceapp/models/state.dart';
import 'package:attendanceapp/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository userRepository;
  UserBloc({@required this.userRepository}) : assert(userRepository != null);

  @override
  UserState get initialState => Uninitialized();

  @override
  Stream<UserState> mapEventToState(UserEvent event) async*{
    if (event is CheckLogin){
      print("started");
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var token = prefs.get("user");
      
      print(token);
      if (token != null){
        print("here");
        var user = prefs.get("user");
        print(user);
        final jsonUser = json.decode(user);

        yield Authenticated(user: User.fromSavedJson(jsonUser));
      }
      else {
        yield NotLoggedIn();
      }
    }
    if(event is SignInUser){
      yield* _mapSignInToState(event);
    }
//    else if(event is SignUpUser) {
//      yield* _mapSignUpToState(event);
//    }
    else if(event is NavigateTo){
      yield NavigateToPage(page: event.page);
    }

  }

  Stream<UserState> _mapSignInToState(SignInUser event) async*{
    yield Loading(msg: 'Logging you in...');

    final StateModel stateModel = await userRepository.loginUser(event.email, event.password);
    if(stateModel is SuccessState) {
      User user = stateModel.value;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("token", user.token);
      prefs.setString("user", json.encode(user));

      yield Authenticated(user:user);
    }
    else if(stateModel is ErrorState){
      yield AuthenticatedError(error: stateModel.msg);
    }
  }
}