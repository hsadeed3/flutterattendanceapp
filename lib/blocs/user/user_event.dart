import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class UserEvent extends Equatable{
  const UserEvent();

  @override
  List<Object> get props => [];
}


class CheckLogin extends UserEvent{

}

class SignInUser extends UserEvent{
  final String email, password;

  const SignInUser({@required this.email, @required this.password}) : assert(email != null), assert(password != null);

  @override
  List<Object> get props => [email, password];

}

class NavigateTo extends UserEvent{
  final String page;

  const NavigateTo({@required this.page}) : assert(page != null);

  @override
  List<Object> get props => [page];
}