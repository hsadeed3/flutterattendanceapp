import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:attendanceapp/models/user.dart';

abstract class UserState extends Equatable{
  const UserState();

  @override
  List<Object> get props => [];
}

class Uninitialized extends UserState {}
class NotLoggedIn extends UserState {}
class Unauthenticated extends UserState {}


class NavigateToPage extends UserState {
  final String page;

  const NavigateToPage({@required this.page}) : assert(page != null);

  @override
  List<Object> get props => [page];
}


class Loading extends UserState {
  final String msg;

  Loading({this.msg});

  @override
  List<Object> get props => [msg];
}

class Authenticated extends UserState {
  final User user;
  Authenticated({this.user});

  @override
  List<Object> get props => [user];
}

class AuthenticatedError extends UserState {
  final String error;

  const AuthenticatedError({@required this.error});

  @override
  List<Object> get props => [error];
}