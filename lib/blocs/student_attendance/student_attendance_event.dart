import 'package:attendanceapp/models/student.dart';
import 'package:equatable/equatable.dart';
import 'dart:io';

abstract class StudentAttendanceEvent extends Equatable{
  const StudentAttendanceEvent();

  @override
  List<Object> get props => [];
}

class StudentAttendanceInitialized extends StudentAttendanceEvent{
  final int studentId;
  StudentAttendanceInitialized({this.studentId});
  @override
  List<Object> get props => [studentId];
}

class SendStudentAttendance extends StudentAttendanceEvent{
  final int attendanceId;
  final String checkInTime;
  final String checkOutTime;
  SendStudentAttendance({this.attendanceId,this.checkInTime,this.checkOutTime});

  @override
  List<Object> get props => [attendanceId,checkInTime,checkOutTime];
}

class RetrieveParentList extends StudentAttendanceEvent {
  final int studentId;
  RetrieveParentList({this.studentId});
  @override
  List<Object> get props => [studentId];
}
class CreateStudentAttendance extends StudentAttendanceEvent{
  final int studentId;
  final String checkInTime;
  final String checkOutTime;
  final String temperature;
  final String date;
  final File photoCheckIn;
  final String notes;
  final String sender;
  CreateStudentAttendance({this.studentId,this.checkInTime,this.checkOutTime,this.temperature,this.sender, this.photoCheckIn, this.notes,this.date});


  @override
  List<Object> get props => [studentId,checkInTime,checkOutTime,temperature,notes,photoCheckIn,date,sender];
}
class UpdateStudentAttendance extends StudentAttendanceEvent {
  final int attendanceId;
  final String checkOutTime;
  final File checkOutPic;
  final String parent;
  final String temperature;

  UpdateStudentAttendance({this.attendanceId,this.checkOutTime,this.checkOutPic,this.parent,this.temperature});

  @override
  List<Object> get props => [attendanceId,checkOutTime,checkOutPic,parent,temperature];
}