import 'package:attendanceapp/models/attendance_info.dart';
import 'package:equatable/equatable.dart';
import 'package:attendanceapp/models/parent.dart';
abstract class StudentAttendanceState extends Equatable{
  const StudentAttendanceState();

  @override
  List<Object> get props => [];
}

class StudentAttendanceUninitialized extends StudentAttendanceState{}

class StudentAttendanceLoading extends StudentAttendanceState{}
class StudentAttendanceLoaded extends StudentAttendanceState{
  final AttendanceInfo attendanceInfo;
  StudentAttendanceLoaded({this.attendanceInfo});

  @override
  List<Object> get props => [attendanceInfo];
}


class StudentAttendanceSuccess extends StudentAttendanceState{}
class StudentAttendanceError extends StudentAttendanceState{
  final String msg;
  StudentAttendanceError({this.msg});

  @override
  List<Object> get props => [msg];
}

