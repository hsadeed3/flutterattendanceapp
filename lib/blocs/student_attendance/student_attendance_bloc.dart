import 'package:attendanceapp/blocs/student_attendance/blocs.dart';
import 'package:attendanceapp/models/attendance_info.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:attendanceapp/models/state.dart';
import 'package:attendanceapp/repositories/student_attendance/repositories.dart';

class StudentAttendanceBloc extends Bloc<StudentAttendanceEvent, StudentAttendanceState>{

  final StudentAttendanceRepository studentAttendanceRepository;
  StudentAttendanceBloc({@required this.studentAttendanceRepository})
      : assert(studentAttendanceRepository != null);


  @override
  Stream<StudentAttendanceState> mapEventToState(StudentAttendanceEvent event) async* {
    if (event is StudentAttendanceInitialized) {
      print('initialized');
      yield StudentAttendanceLoading();
      try {
        final AttendanceInfo attendanceInfo = await studentAttendanceRepository
            .getStudentAttendanceInfo(event.studentId);
        yield StudentAttendanceLoaded(attendanceInfo: attendanceInfo);
      } catch (_) {
        yield StudentAttendanceError();
      }
    }
    if (event is CreateStudentAttendance) {
      yield StudentAttendanceLoading();
      final StateModel attendanceInfoModel = await this
          .studentAttendanceRepository.createNewAttendance(
          event.studentId, event.checkInTime, event.checkOutTime, event.temperature,event.date, event.photoCheckIn, event.notes, event.sender,event.temperature);
      if (attendanceInfoModel is SuccessState) {
        yield StudentAttendanceSuccess();
      }
      else if (attendanceInfoModel is ErrorState) {
        yield StudentAttendanceError(msg: attendanceInfoModel.msg);
      }
    }

      if (event is UpdateStudentAttendance) {
        yield StudentAttendanceLoading();
        final StateModel attendanceInfoModel = await this.studentAttendanceRepository.sendAttendanceInfo(event.attendanceId,  event.checkOutTime, event.checkOutPic, event.parent,event.temperature);
        if (attendanceInfoModel is SuccessState) {
          yield StudentAttendanceSuccess();
        }
        else if (attendanceInfoModel is ErrorState) {
          yield StudentAttendanceError(msg: attendanceInfoModel.msg);
        }
      }

  }

  @override
  StudentAttendanceState get initialState => StudentAttendanceUninitialized();

}