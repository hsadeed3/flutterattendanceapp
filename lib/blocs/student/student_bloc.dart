import 'package:attendanceapp/models/attendance_info.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:attendanceapp/models/student.dart';
import 'package:attendanceapp/models/parent.dart';
import 'package:attendanceapp/blocs/student/student_event.dart';
import 'package:attendanceapp/blocs/student/student_state.dart';
import 'package:attendanceapp/repositories/student/student_repository.dart';

class StudentBloc extends Bloc<StudentEvent,StudentState>{
  final VisitorRepository studentRepository;

   StudentBloc({@required this.studentRepository})
      : assert(studentRepository != null);

  @override
  StudentState get initialState => StudentEmpty();

  @override
  Stream<StudentState> mapEventToState(StudentEvent event) async* {
    if (event is FetchStudent) {
      yield StudentLoading();
      try {
        final List<Student> students = await studentRepository.getTodayClassroomAttendance(event.classId);
        yield StudentLoaded(students: students);
      } catch (_) {
        yield StudentError();
      }
    }
    if (event is RetrieveParentList){
      yield StudentLoading();
      try {
        final List<Parent> parents = await this.studentRepository.getStudentParent(event.studentId);
        yield ParentLoaded(parents:parents);
      }
      catch (_) {
        yield StudentError();
      }
    }
  }
}