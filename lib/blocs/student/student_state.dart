import 'package:attendanceapp/models/attendance_info.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:attendanceapp/models/parent.dart';
import 'package:attendanceapp/models/student.dart';
abstract class StudentState extends Equatable {
  const StudentState();
  @override
  List<Object> get props => [];
}

class StudentEmpty extends StudentState {}

class StudentLoading extends StudentState {}

class StudentLoaded extends StudentState {
  final List<Student> students;

  const StudentLoaded({@required this.students}) : assert(students != null);

  @override
  List<Object> get props => [students];
}
class StudentError extends StudentState {}


class ParentLoaded extends StudentState {
  final List<Parent> parents;

  const ParentLoaded({this.parents}) : assert(parents!=null);

  @override
  List<Object> get props => [parents];
}
class RetrieveParentLoaded extends StudentState {
  final List<Parent> parents;

  RetrieveParentLoaded({this.parents});
  @override
  List<Object> get props => [parents];

}