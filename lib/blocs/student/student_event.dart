import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class StudentEvent extends Equatable {
  const StudentEvent();
}

class FetchStudent extends StudentEvent {
  final int classId;

  const FetchStudent({@required this.classId}) : assert(classId!=null);

  @override
  List<Object> get props => [classId];

}

class RetrieveParentList extends StudentEvent {
  final int studentId;
  RetrieveParentList({this.studentId});
  @override
  List<Object> get props => [studentId];
}