import 'package:attendanceapp/models/attendance_info.dart';
import 'package:equatable/equatable.dart';

abstract class VisitorState extends Equatable{
  const VisitorState();

  @override
  List<Object> get props => [];
}

class VisitorInitialized extends VisitorState{}

class VisitorLoading extends VisitorState{}

class VisitorSuccess extends VisitorState{

}
class VisitorError extends VisitorState{
  final String msg;
  VisitorError({this.msg});

  @override
  List<Object> get props => [msg];
}

