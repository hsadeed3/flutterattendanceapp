import 'package:attendanceapp/blocs/visitor/blocs.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:attendanceapp/models/state.dart';
import 'package:attendanceapp/repositories/visitor/repositories.dart';

class VisitorBloc extends Bloc<VisitorEvent, VisitorState>{

  final VisitorRepository visitorRepository;
  VisitorBloc({@required this.visitorRepository})
      : assert(visitorRepository != null);

  @override
  Stream<VisitorState> mapEventToState(VisitorEvent event) async* {

    if (event is SendVisitorAttendance) {
      yield VisitorLoading();
      final StateModel attendanceInfoModel = await this
          .visitorRepository.registerVisitor(
          event.name, event.address, event.contact, event.purpose,event.temperature);
      if (attendanceInfoModel is SuccessState) {
        yield VisitorSuccess();
      }
      else if (attendanceInfoModel is ErrorState) {
        yield VisitorError(msg: attendanceInfoModel.msg);
      }
    }
  }

  @override
  VisitorState get initialState => VisitorInitialized();

}