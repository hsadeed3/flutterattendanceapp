import 'package:equatable/equatable.dart';

abstract class VisitorEvent extends Equatable{
  const VisitorEvent();

  @override
  List<Object> get props => [];
}

class SendVisitorAttendance extends VisitorEvent{
  final String name;
  final String address;
  final String contact;
  final String purpose;
  final String temperature;
  SendVisitorAttendance({this.name, this.address, this.contact, this.purpose, this.temperature});
  @override
  List<Object> get props => [name,address,contact,purpose,temperature];
}
