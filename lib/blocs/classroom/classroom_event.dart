import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ClassroomEvent extends Equatable {
  const ClassroomEvent();
}

class FetchClassroom extends ClassroomEvent {
  final int schoolId;

  const FetchClassroom({@required this.schoolId}) : assert(schoolId!=null);

  @override
  List<Object> get props => [schoolId];

}