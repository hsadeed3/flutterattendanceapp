import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:attendanceapp/models/classroom.dart';

abstract class ClassroomState extends Equatable {
  const ClassroomState();
  @override
  List<Object> get props => [];
}

class ClassroomEmpty extends ClassroomState {}

class ClassroomLoading extends ClassroomState {}

class ClassroomLoaded extends ClassroomState {
  final List<Classroom> classrooms;

  const ClassroomLoaded({@required this.classrooms}) : assert(classrooms != null);

  @override
  List<Object> get props => [classrooms];
}
class ClassroomError  extends ClassroomState {}