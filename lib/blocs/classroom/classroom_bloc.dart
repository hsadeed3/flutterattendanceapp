import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:attendanceapp/models/classroom.dart';

import 'package:attendanceapp/blocs/classroom/classroom_event.dart';
import 'package:attendanceapp/blocs/classroom/classroom_state.dart';
import 'package:attendanceapp/repositories/classroom/classroom_repository.dart';

class ClassroomBloc extends Bloc<ClassroomEvent,ClassroomState>{
  final ClassroomRepository classroomRepository;

  ClassroomBloc({@required this.classroomRepository})
      : assert(classroomRepository != null);

  @override
  ClassroomState get initialState => ClassroomEmpty();

  @override
  Stream<ClassroomState> mapEventToState(ClassroomEvent event) async* {
    if (event is FetchClassroom) {
      yield ClassroomLoading();
      try {
        final List<Classroom> classrooms = await classroomRepository.getStudentByClass(event.schoolId);
        yield ClassroomLoaded(classrooms: classrooms);
      } catch (_) {
        yield ClassroomError();
      }
    }
  }
}