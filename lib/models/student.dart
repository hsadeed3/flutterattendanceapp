import 'package:attendanceapp/widgets/student/student.dart';
import 'package:equatable/equatable.dart';

class Student extends Equatable {

  final int studentId;
  final String studentName;
  final String studentImage;
  final String nickname;
  final String gender;
  final int age;
  final String checkInTime;
  final String checkOutTime;

  const Student({
    this.studentId,
    this.studentName,
    this.studentImage,
    this.nickname,
    this.gender,
    this.age,
    this.checkInTime,
    this.checkOutTime,
  });

  @override
  List<Object> get props => [
    studentId,
    studentName,
    studentImage,
    gender,
    nickname,
    age,
    checkInTime,
    checkOutTime
  ];
  static Student fromJson(dynamic json) {
    var checkInTime = "";
    var checkOutTime = "";
    if (json["attendance"] != null){

      checkInTime = json["attendance"]["check_in_time"];
      checkOutTime = json["attendance"]["check_out_time"] != null ? json["attendance"]["check_out_time"]: "";
    }
    return Student(
      studentId: json['student_id'],
      studentName: json['student_name'],
      studentImage: json['student_image'],
      nickname: json['nickname'],
      gender: json['gender'],
      age: json['age'],
      checkInTime: checkInTime,
      checkOutTime: checkOutTime
    );
  }

  static List<Student> listFromJson(dynamic json) {
    var results = json["data"];

    if (results!= null){
      var finalResults = new List<Student>();
      results.forEach((v){
        finalResults.add(Student.fromJson(v));
      });
      return finalResults;
    }
    return List<Student>();
  }
}