import 'package:equatable/equatable.dart';

class Institute extends Equatable {

  final String instituteName;
  final int id;

  const Institute({
    this.id,
    this.instituteName,
  });

  @override
  List<Object> get props =>
      [
        id,
        instituteName,

      ];

  static Institute fromJson(dynamic json) {
    return Institute(
      id: json['institute_id'],
      instituteName: json['institute_name'],
    );
  }
}