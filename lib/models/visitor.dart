import 'package:equatable/equatable.dart';

class Visitor extends Equatable {

  final String name;
  final String address;
  final String contact;
  final String purpose;
  final String temperature;
  final String schoolId;


  const Visitor({
    this.name,
    this.address,
    this.contact,
    this.purpose,
    this.temperature,
    this.schoolId
  });

  @override
  List<Object> get props => [
    name,
    address,
    contact,
    purpose,
    temperature,
    schoolId

  ];
  static Visitor fromJson(dynamic json) {
    return Visitor(
        name: json['name'],
        address:  json['address'],
      contact: json['contact'],
      purpose: json['purpose'],
      temperature: json['temperature'],
      schoolId: json['school_id']
    );
  }

}