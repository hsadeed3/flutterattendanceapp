import 'package:attendanceapp/widgets/student/student.dart';
import 'package:equatable/equatable.dart';

class AttendanceInfo extends Equatable {

  final String checkInTime;
  final String checkOutTime;
  final String notes;
  final int studentId;
  final int teacherId;
  final int attendanceId;
  final String temperature;
  final String temperatureout;
  final String photoCheckIn;
  final String photoCheckOut;

  const AttendanceInfo({
    this.checkInTime,
    this.checkOutTime,
    this.notes,
    this.studentId,
    this.attendanceId,
    this.temperature,
    this.temperatureout,
    this.photoCheckIn,
    this.teacherId,
    this.photoCheckOut
  });

  @override
  List<Object> get props => [
    checkInTime,
    checkOutTime,
    notes,
    studentId,
    attendanceId,
    temperature,
    temperatureout,
    photoCheckIn,
    teacherId,
    photoCheckOut
  ];
  static AttendanceInfo fromJson(dynamic json) {
    var jsonData = json["data"];
    var studentId = 0;
    var teacherId = 0 ;

    if (jsonData['student_id'] != null ){
      print("here");
      studentId = int.parse(jsonData['student_id']);
    }
    else {
      if (jsonData['teacher_id'] is int) {
        teacherId = jsonData['teacher_id'];
      }
      else {
        teacherId = int.parse(jsonData['teacher_id']);
      }
    }
    return AttendanceInfo(
        checkInTime: jsonData['check_in_time'],
        checkOutTime: jsonData['check_out_time'],
        notes: jsonData['notes'],
        studentId: studentId,
        teacherId: teacherId,
        attendanceId: jsonData["attendance_id"],
        temperature: jsonData['temperature'],
        temperatureout: jsonData['check_out_temperature'],
        photoCheckIn: jsonData['photo_check_in'],
        photoCheckOut: jsonData['photo_check_out']
    );
  }

  static List<AttendanceInfo> listFromJson(dynamic json) {
    var results = json["data"];
    if (results!= null){
      var finalResults = new List<AttendanceInfo>();
      results.forEach((v){
        finalResults.add(AttendanceInfo.fromJson(v));
      });
      return finalResults;
    }
    return List<AttendanceInfo>();
  }
}