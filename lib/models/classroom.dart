import 'package:equatable/equatable.dart';

class Classroom extends Equatable {

  final String classroomName;
  final int id;

  const Classroom({
    this.id,
    this.classroomName,
  });

  @override
  List<Object> get props => [
    id,
    classroomName,

  ];
  static Classroom fromJson(dynamic json) {
    return Classroom(
      id:json['class_id'],
      classroomName: json['class_name'],
    );
  }

  static List<Classroom> listFromJson(dynamic json) {
    var results = json["data"];
    if (results!= null){
      var finalResults = new List<Classroom>();
      results.forEach((v){
        finalResults.add(Classroom.fromJson(v));
      });
      return finalResults;
    }
    return List<Classroom>();
  }
}