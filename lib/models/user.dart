import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int id;
  String name,token;

  User({
    this.id,
    this.name,
    this.token
  });
  @override
  List<Object> get props => [
  id,
    name,
    token
  ];

  static User fromJson(dynamic json){
    return User(
      id:json['institute']['id'],
      name:json['institute']['name'],
      token: json['token']
    );
  }

  static User fromSavedJson(dynamic json){
    return User(
        id:json['id'],
        name:json['name'],
        token: json['token']
    );
  }


  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'token': token,
  };
}