import 'package:equatable/equatable.dart';

class Parent extends Equatable {

  final String parentName;
  final String parentId;
  final String imgUrl;

  const Parent({
    this.parentName,
    this.parentId,
    this.imgUrl
  });

  @override
  List<Object> get props => [
    parentName,
    parentId,
    imgUrl
  ];
  static Parent fromJson(dynamic json) {
    return Parent(
        parentName: json['name'],
        parentId:  json['id'] is int ? json["id"].toString() : json["id"],
        imgUrl: json['pic_url']
    );
  }

  static List<Parent> listFromJson(dynamic json) {

    var results = json["data"];
    if (results!= null){

      var finalResults = new List<Parent>();

      results.forEach((v){
        finalResults.add(Parent.fromJson(v));
      });

      var resultAS = json["authorized_ senders"];
      resultAS.forEach((v){
        finalResults.add(Parent.fromJson(v));
      });

      return finalResults;
    }
    return List<Parent>();
  }
}