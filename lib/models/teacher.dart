import 'package:equatable/equatable.dart';

class Teacher extends Equatable {
  final String teacherName;
  final int teacherId;
  final String checkInTime;
  final String checkOutTime;
  final String picUrl;

  const Teacher(
      {this.teacherName, this.teacherId, this.checkInTime, this.checkOutTime,this.picUrl});

  @override
  List<Object> get props => [teacherName, teacherId, checkInTime, checkOutTime];
  static Teacher fromJson(dynamic json) {
    var checkInTime = "";
    var checkOutTime = "";
    if (json["attendance"] != null) {
      checkInTime = json["attendance"]["check_in_time"];
      checkOutTime = json["attendance"]["check_out_time"] != null
          ? json["attendance"]["check_out_time"]
          : "";
    }
    return Teacher(
        teacherName: json['name'],
        teacherId: json['id'],
        picUrl:json['pic_url'],
        checkInTime: checkInTime,
        checkOutTime: checkOutTime);
  }

  static List<Teacher> listFromJson(dynamic json) {
    var results = json["data"];
    if (results != null) {
      var finalResults = new List<Teacher>();
      results.forEach((v) {
        finalResults.add(Teacher.fromJson(v));
      });
      return finalResults;
    }
    return List<Teacher>();
  }
}
