import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Visitor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
          title: Text('New Visitor Info'), backgroundColor: Colors.amber[600]),
      body: SingleChildScrollView(child: VisitorForm()),
    );
  }
}

class VisitorForm extends StatefulWidget {
  @override
  _VisitorFormState createState() => _VisitorFormState();
}

class _VisitorFormState extends State<VisitorForm> {
  final _formKey = GlobalKey<FormState>();
  DateTime _currentdate = new DateTime.now();
  DateTime _currenttime = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    String _formatthedate = new DateFormat.yMMMd().format(_currentdate);
    String _formatthetime = new DateFormat.jm().format(_currenttime);

    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(50.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Date:$_formatthedate',
                style: TextStyle(color: Colors.black, fontSize: 20)),
            SizedBox(height: 10),
            Text('Time:$_formatthetime',
                style: TextStyle(color: Colors.black, fontSize: 20)),
            SizedBox(height: 10),
            TextFormField(
                decoration: InputDecoration(
                    labelText: 'Name', filled: true, fillColor: Colors.white),
                style: TextStyle(fontSize: 25),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter visitor name';
                  }
                  return null;
                }),
            SizedBox(height: 10),
            TextFormField(
                decoration: InputDecoration(
                    labelText: 'Contact Number',
                    filled: true,
                    fillColor: Colors.white),
                style: TextStyle(fontSize: 25),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter contact number';
                  }
                  return null;
                }),
            SizedBox(height: 10),
            TextFormField(
                decoration: InputDecoration(
                    labelText: 'Address',
                    filled: true,
                    fillColor: Colors.white),
                style: TextStyle(fontSize: 25),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter Address';
                  }
                  return null;
                }),
            SizedBox(height: 10),
            TextFormField(
                decoration: InputDecoration(
                    labelText: 'Purpose of Visit',
                    filled: true,
                    fillColor: Colors.white),
                style: TextStyle(fontSize: 25),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter purpose of visit';
                  }
                  return null;
                }),
            SizedBox(height: 10),
            TextFormField(
                decoration: InputDecoration(
                    labelText: 'Temperature',
                    filled: true,
                    fillColor: Colors.white),
                style: TextStyle(fontSize: 25),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter temperature';
                  }
                  return null;
                }),
            SizedBox(height: 25),
            Row(
              children: <Widget>[
                FlatButton(
                    child: Text('Cancel',
                        style: TextStyle(
                            color: Colors.lightBlueAccent, fontSize: 30)),
                    color: Colors.white,
                    padding: EdgeInsets.all(10),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                SizedBox(width: 25),
                FlatButton(
                  child: Text('Submit',
                      style: TextStyle(color: Colors.white, fontSize: 30)),
                  color: Colors.lightBlueAccent,
                  padding: EdgeInsets.all(10),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Scaffold.of(context)
                          .showSnackBar(SnackBar(content: Text('Processing')));
                    }
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
