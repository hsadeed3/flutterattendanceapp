import 'package:attendanceapp/utils/blecontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

class tempUIPage extends StatefulWidget {

  tempUIPage();

  @override
  State<tempUIPage> createState() => _tempUIPageState();
}

class _tempUIPageState extends State<tempUIPage> {
  TextStyle myStyle = TextStyle(fontSize: 30);

  final BleController bleCtrl = Get.put(BleController(""));

  @override Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(title: Text('Digital Thermometer'),backgroundColor: Colors.orange,
        ),
        body: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,

              children: <Widget>[


          new Image.asset(

          'assets/logos/thermoMeter.jpg',

            width: 250.0,
            height: 250.0,
            fit: BoxFit.cover,
          ),
      Obx(() => Text('STATUS:${bleCtrl.status}',
            style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color:Colors.red))),
      SizedBox(height: 20),
      Text('🌡️',style: TextStyle(fontSize: 80),),
      Obx(() => Text('TEMPERATURE: ${bleCtrl.temperature}',
          style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.red),)),
      SizedBox(height: 50),

      SizedBox(height: 15.0),
      ElevatedButton(

            style: ElevatedButton.styleFrom(
              onPrimary: Colors.black87,
              primary: Colors.orange[300],
              minimumSize: Size(100, 50),
              padding: EdgeInsets.symmetric(horizontal: 18),
            ),
            onPressed: () => {

              setState(() {
                bleCtrl.temperature=' '.obs;
              }),
            },
            child: Text('REFRESH SCAN',
                style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold))
      ),]),
        ));}}
