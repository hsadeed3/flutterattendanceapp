import 'package:attendanceapp/healthreport.dart';
import 'package:attendanceapp/models/parent.dart';
import 'package:attendanceapp/widgets/student_attendance/studentdetail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/models/student.dart';
import 'package:attendanceapp/blocs/student/blocs.dart';
import 'utils/observe_state.dart';
class FetchingDetail extends StatefulWidget {
 final Student student;
  FetchingDetail({this.student});
  @override
  _FetchingDetailState createState() => _FetchingDetailState();
}

class _FetchingDetailState extends State<FetchingDetail> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Select Sender'),
          backgroundColor: Colors.amber[600],
        ),
        body: Center(child:
            BlocBuilder<StudentBloc, StudentState>(builder: (context, state) {
          if (state is StudentEmpty) {
            BlocProvider.of<StudentBloc>(context)
                .add(RetrieveParentList(studentId: widget.student.studentId));
          } else if (state is StudentLoading) {
            return CircularProgressIndicator();
          } else if (state is ParentLoaded) {
            List<Parent> parents = [];
            parents = state.parents;
            parents.add(Parent(parentName: "Others", parentId: "0"));
            print(parents);
            return Column(children: <Widget>[
              SizedBox(height: 20),
              Container(
                height: 400,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: parents.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      child: Column(
                        children: <Widget>[
                          parents[index].imgUrl != null && parents[index].imgUrl !="" ?Image.network(parents[index].imgUrl,height: 300,): Image.asset('assets/logos/dad.png', height:300),
                          SizedBox(height: 10),
                          Text(parents[index].parentName,style: TextStyle(fontSize: 30),)
                        ],
                      ),
                      onTap: () {

                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => WoundChart()),
                        // );
                        //
                       studentData['parent'] = parents[index];
                       StateProvider _stateProvider = StateProvider();
                       _stateProvider.notify(ObserverState.RELOAD_STUDENT_RECORD);
                       Navigator.popUntil(context, ModalRoute.withName('/studentDetail'));

                       // Navigator.popUntil(context,  ModalRoute.withName('/studentDetail'));
                      //  Navigator.popUntil(context, ModalRoute.withName('/studentDetail'));
                       // Navigator.pop(context, {'parent': parents[index]});
                      },

                    );
                  },
                ),
              ),
              SizedBox(height: 20),
            ]);
          }
          return Container();
        })));
  }
}
