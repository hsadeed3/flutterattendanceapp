import 'package:attendanceapp/blocs/teacher/blocs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'utils/observe_state.dart';


class MoreInfo extends StatefulWidget {
  @override
  _MoreInfoState createState() => _MoreInfoState();
}

class _MoreInfoState extends State<MoreInfo> {
  int _milk = 0;
  int _bottle = 0;
  int _clothes = 0;
  int _diapers = 0;
  int _towel = 0;
  int _plastic = 0;

  void _milkRemove() {
    setState(() {
      _milk--;
    });
  }

  void _milkAdd() {
    setState(() {
      _milk++;
    });
  }

  void _bottleRemove() {
    setState(() {
      _bottle--;
    });
  }

  void _bottleAdd() {
    setState(() {
      _bottle++;
    });
  }

  void _clothesRemove() {
    setState(() {
      _clothes--;
    });
  }

  void _clothesAdd() {
    setState(() {
      _clothes++;
    });
  }

  void _diapersRemove() {
    setState(() {
      _diapers--;
    });
  }

  void _diapersAdd() {
    setState(() {
      _diapers++;
    });
  }

  void _towelRemove() {
    setState(() {
      _towel--;
    });
  }

  void _towelAdd() {
    setState(() {
      _towel++;
    });
  }

  void _plasticRemove() {
    setState(() {
      _plastic--;
    });
  }

  void _plasticAdd() {
    setState(() {
      _plastic++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber[600],
          title: Text('More Information'),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Image.asset('assets/logos/cleanup.png',
                                width: 100, height: 100),
                            Text('Cleaned up?', style: TextStyle(fontSize: 20))
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          Container(
                              width: 90,
                              padding: EdgeInsets.all(8),
                              color: Colors.blue,
                              child: GestureDetector(
                                  child: Text(
                                'Yes',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                                textAlign: TextAlign.center,
                              ))),
                          Container(
                              width: 90,
                              padding: EdgeInsets.all(8),
                              color: Colors.red,
                              child: GestureDetector(
                                  child: Text(
                                'No',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                                textAlign: TextAlign.center,
                              ))),
                        ],
                      )
                    ],
                  ),
                  SizedBox(width: 10),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Image.asset('assets/logos/breakfast.png',
                                width: 100, height: 100),
                            Text('Had breakfast?',
                                style: TextStyle(fontSize: 20)),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          Container(
                              width: 100,
                              padding: EdgeInsets.all(8),
                              color: Colors.blue,
                              child: GestureDetector(
                                  child: Text(
                                'Yes',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                                textAlign: TextAlign.center,
                              ))),
                          Container(
                              width: 100,
                              padding: EdgeInsets.all(8),
                              color: Colors.red,
                              child: GestureDetector(
                                  child: Text(
                                'No',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                                textAlign: TextAlign.center,
                              ))),
                        ],
                      )
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Image.asset('assets/logos/sleep.png',
                                width: 100, height: 100),
                            Text('Sleep well?', style: TextStyle(fontSize: 20)),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          Container(
                              width: 100,
                              padding: EdgeInsets.all(8),
                              color: Colors.blue,
                              child: GestureDetector(
                                  child: Text(
                                'Yes',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                                textAlign: TextAlign.center,
                              ))),
                          Container(
                              width: 100,
                              padding: EdgeInsets.all(8),
                              color: Colors.red,
                              child: GestureDetector(
                                  child: Text(
                                'No',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                                textAlign: TextAlign.center,
                              ))),
                        ],
                      )
                    ],
                  ),
                  SizedBox(width: 10),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Image.asset('assets/logos/diseases.png',
                                width: 100, height: 100),
                            Text('Diseases?', style: TextStyle(fontSize: 20)),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          Container(
                              width: 100,
                              padding: EdgeInsets.all(8),
                              color: Colors.blue,
                              child: GestureDetector(
                                  child: Text(
                                'Yes',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                                textAlign: TextAlign.center,
                              ))),
                          Container(
                              width: 100,
                              padding: EdgeInsets.all(8),
                              color: Colors.red,
                              child: GestureDetector(
                                  child: Text(
                                'No',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                                textAlign: TextAlign.center,
                              ))),
                        ],
                      )
                    ],
                  )
                ],
              ),
              SizedBox(height: 10),
              Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        Image.asset('assets/logos/healthy.png',
                            width: 100, height: 100),
                        SizedBox(height: 20),
                        Text('Healthy?', style: TextStyle(fontSize: 20)),
                      ],
                    ),
                  ),
                  SizedBox(height: 10, width: 50),
                  Row(
                    children: <Widget>[
                      Container(
                          width: 100,
                          padding: EdgeInsets.all(8),
                          color: Colors.blue,
                          child: GestureDetector(
                              child: Text(
                            'Yes',
                            style: TextStyle(
                                color: Colors.white, fontSize: 18),
                            textAlign: TextAlign.center,
                          ))),
                      Container(
                          width: 100,
                          padding: EdgeInsets.all(8),
                          color: Colors.red,
                          child: GestureDetector(
                              child: Text(
                            'No',
                            style: TextStyle(
                                color: Colors.white, fontSize: 18),
                            textAlign: TextAlign.center,
                          ))),
                    ],
                  )
                ],
              ),
              SizedBox(height: 50),
              Text('List of medicine', style: TextStyle(fontSize: 20)),
              Container(
                width: 500,
                height: 40,
                color: Colors.grey[350],
                child: TextField(
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.text,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(8.0))),
              ),
              SizedBox(height: 40),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(width: 40),
                          Image.asset('assets/logos/milkbottle.png',
                              width: 70, height: 70),
                          SizedBox(width: 40),
                          Text('Milk bottle', style: TextStyle(fontSize: 20)),
                          SizedBox(width: 10),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                              heroTag: null,
                                backgroundColor: Colors.red,
                                onPressed: _milkRemove,
                                tooltip: 'Decrement',
                                child: Icon(Icons.remove)),
                          ),
                          SizedBox(width: 10),
                          Text(
                            '$_milk',
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                onPressed: _milkAdd,
                                tooltip: 'Increment',
                                child: Icon(Icons.add)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(width: 40),
                          Image.asset('assets/logos/bottle.png',
                              width: 70, height: 70),
                          SizedBox(width: 40),
                          Text('Bottle', style: TextStyle(fontSize: 20)),
                          SizedBox(width: 50),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                backgroundColor: Colors.red,
                                onPressed: _bottleRemove,
                                tooltip: 'Decrement',
                                child: Icon(Icons.remove)),
                          ),
                          SizedBox(width: 10),
                          Text(
                            '$_bottle',
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                onPressed: _bottleAdd,
                                tooltip: 'Increment',
                                child: Icon(Icons.add)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height : 15),
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(width: 40),
                          Image.asset('assets/logos/clothes.png',
                              width: 70, height: 70),
                          SizedBox(width: 40),
                          Text('Clothes', style: TextStyle(fontSize: 20)),
                          SizedBox(width: 35),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                backgroundColor: Colors.red,
                                onPressed: _clothesRemove,
                                tooltip: 'Decrement',
                                child: Icon(Icons.remove)),
                          ),
                          SizedBox(width: 10),
                          Text(
                            '$_clothes',
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                onPressed: _clothesAdd,
                                tooltip: 'Increment',
                                child: Icon(Icons.add)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(width: 40),
                          Image.asset('assets/logos/diapers.png',
                              width: 70, height: 70),
                          SizedBox(width: 40),
                          Text('Diaper', style: TextStyle(fontSize: 20)),
                          SizedBox(width: 45),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                backgroundColor: Colors.red,
                                onPressed: _diapersRemove,
                                tooltip: 'Decrement',
                                child: Icon(Icons.remove)),
                          ),
                          SizedBox(width: 10),
                          Text(
                            '$_diapers',
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                onPressed: _diapersAdd,
                                tooltip: 'Increment',
                                child: Icon(Icons.add)),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                          Row(
                            children: <Widget>[
                          SizedBox(width: 40),
                          Image.asset('assets/logos/towel.png',
                              width: 70, height: 70),
                          SizedBox(width: 40),
                          Text('Towel', style: TextStyle(fontSize: 20)),
                          SizedBox(width: 50),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                backgroundColor: Colors.red,
                                onPressed: _towelRemove,
                                tooltip: 'Decrement',
                                child: Icon(Icons.remove)),
                          ),
                          SizedBox(width: 10),
                          Text(
                            '$_towel',
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                onPressed: _towelAdd,
                                tooltip: 'Increment',
                                child: Icon(Icons.add)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(width: 40),
                          Image.asset('assets/logos/plastic.png',
                              width: 70, height: 70),
                          SizedBox(width: 40),
                          Text('Plastic', style: TextStyle(fontSize: 20)),
                          SizedBox(width: 40),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                backgroundColor: Colors.red,
                                onPressed: _plasticRemove,
                                tooltip: 'Decrement',
                                child: Icon(Icons.remove)),
                          ),
                          SizedBox(width: 10),
                          Text(
                            '$_plastic',
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 30,
                            width: 30,
                            child: FloatingActionButton(
                                heroTag: null,
                                onPressed: _plasticAdd,
                                tooltip: 'Increment',
                                child: Icon(Icons.add)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 20),
              FlatButton(
                child: Text('Submit',
                    style: TextStyle(fontSize: 20, color: Colors.white)),
                color: Colors.amber[600],
                padding: EdgeInsets.all(8),
                onPressed: () {
                  StateProvider _stateProvider = StateProvider();
                  _stateProvider.notify(ObserverState.RELOAD_STUDENT_RECORD);
                  Navigator.popUntil(context, ModalRoute.withName('/studentDetail'));
                },
              )

            ],
          ),
        ));
  }
}
