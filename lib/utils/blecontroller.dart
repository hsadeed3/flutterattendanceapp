import 'dart:async';
import 'dart:developer';
import 'package:attendanceapp/RegistertThermometer.dart';
import 'package:attendanceapp/models/bleModels.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:attendanceapp/commons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:attendanceapp/RegistertThermometer.dart';
class BleController {
  String message;

  BleController(String m){
    this.message = m;
  }

  static Future init() async {
    prefs = await SharedPreferences.getInstance();
  }


  final reg=InitiateRegistration();

  final frb = FlutterReactiveBle();


  String deviceID=macIdSave;
  StreamSubscription<ConnectionStateUpdate> connection;
  QualifiedCharacteristic rx;
  RxString status ='DEVICE NOT CONNECTED'.obs;
  RxString temperature = ' '.obs;

  void connect() async {
log("Hi;");
log(macIdSave);

    connection = frb.connectToDevice(id: prefs.getString('string') ?? '', connectionTimeout: const Duration(seconds: 5)).listen((state) {
      if (state.connectionState == DeviceConnectionState.connected) {

        status.value = 'CONNECTED!';

        // get rxm
        rx = QualifiedCharacteristic(
            serviceId: Uuid.parse("0000fff0-0000-1000-8000-00805f9b34fb"),
            characteristicId:
                Uuid.parse("0000fff1-0000-1000-8000-00805f9b34fb"),
            deviceId:prefs.getString('string') ?? '');


        // subscribe to rx
// ignore: unnecessary_statements

        frb.subscribeToCharacteristic(rx).listen((data)  {
          print("Hello");

          print(data);
          print(data.elementAt(5));
          print(data.elementAt(6));
          print((data.elementAt(4) * 256 + data.elementAt(5)) / 10);
          double final_temp =
              (data.elementAt(4) * 256 + data.elementAt(5)) / 10;
          String final_temp_value = final_temp.toString();
          temperature.value = final_temp_value;


        });
      }
      else{
        status.value='DEVICE NOT CONNECTED';
      }


  });
  }

  void disconnect() async {
    connection.cancel();
  }
}
