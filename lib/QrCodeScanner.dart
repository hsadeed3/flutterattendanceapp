

import 'dart:async';

import 'package:attendanceapp/blocs/student/blocs.dart';
import 'package:attendanceapp/repositories/student_attendance/student_attendance_client.dart';
import 'package:attendanceapp/repositories/student_attendance/student_attendance_repository.dart';
import 'package:attendanceapp/widgets/student/student_item.dart';
import 'package:attendanceapp/widgets/student_attendance/studentdetail.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


import 'UserData.dart';
import 'blocs/student_attendance/student_attendance_bloc.dart';
import 'models/student.dart';
import 'models/user.dart';

import 'package:http/http.dart' as http;


class QrCodeScanner extends StatefulWidget {
  get DiryDobject => null;

  @override
  _QrCodeScannerState createState() => _QrCodeScannerState(DiryDobject);
}

class _QrCodeScannerState extends State<QrCodeScanner> {
  String _scanBarcode = 'Unknown';
  final Student student;

  _QrCodeScannerState(this.student);

  @override
  void initState() {
    super.initState();
  }

  Future<void> startBarcodeScanStream() async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
        '#ff6666', 'Cancel', true, ScanMode.BARCODE)
        .listen((barcode) => print(barcode));
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> scanBarcodeNormal() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.BARCODE);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  @override
  Widget build(BuildContext context) {

    final StudentAttendanceRepository attendanceRepository =
    StudentAttendanceRepository(
        studentAttendanceClient: StudentAttendanceClient(
          httpClient: http.Client(),
        ));
    return MaterialApp(
        home: Scaffold(

            appBar: AppBar(title: const Text('Please Check In')),

            body: Builder(builder: (BuildContext context) {

              return Container(
                  alignment: Alignment.center,
                  child: Flex(
                      direction: Axis.vertical,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        ElevatedButton(

                            onPressed: () => {

                              scanQR(),
                            Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => UserData()),
                            ),

                        },
                            child: Text('Start QR scan')),

                        Text('Scan result : $_scanBarcode\n',
                            style: TextStyle(fontSize: 20))
                      ]));
            })));
  }
}
