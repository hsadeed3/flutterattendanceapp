import 'package:attendanceapp/models/classroom.dart';
import 'package:attendanceapp/widgets/student/student_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/blocs/student/blocs.dart';

class Student extends StatelessWidget {
  final Classroom classroom ;
  Student({this.classroom});
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.amber[600],
          title: Text(classroom.classroomName),
        ),
        body:
        Center(

          child: BlocBuilder<StudentBloc, StudentState>(
            builder: (context, state) {
              if (state is StudentEmpty) {
                print(this.classroom);
                BlocProvider.of<StudentBloc>(context).add(FetchStudent(classId: this.classroom.id));
                return Center(child: Text('Please Wait'));
              } else if (state is StudentLoading) {
                return Center(child: CircularProgressIndicator());
              } else if (state is StudentLoaded) {
                final students = state.students;
                return ListView.builder(
                  itemCount: students.length,
                  itemBuilder: (context, position) {
                    return StudentItem(student:students[position], classroom: classroom,);
                  },
                );
              } else {
                return Text(
                  'Something went wrong!',
                  style: TextStyle(color: Colors.red),
                );
              }
            },
          ),
        ));
  }
}
