import 'package:attendanceapp/blocs/student/blocs.dart';
import 'package:attendanceapp/models/attendance_info.dart';
import 'package:attendanceapp/widgets/student_attendance/studentdetail.dart';
import 'package:flutter/material.dart';
import 'package:attendanceapp/models/student.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/blocs/student_attendance/blocs.dart';
import 'package:attendanceapp/repositories/student_attendance/repositories.dart';
import 'package:http/http.dart' as http;

import '../../models/classroom.dart';

class StudentItem extends StatelessWidget {
  final Student student;
  final Classroom classroom;
  StudentItem({this.student, this.classroom});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final StudentAttendanceRepository attendanceRepository =
            StudentAttendanceRepository(
          studentAttendanceClient: StudentAttendanceClient(
            httpClient: http.Client(),
          ),
        );
        Navigator.push(
            context,
            MaterialPageRoute(
              settings: RouteSettings(name: '/studentDetail'),
              builder: (context) => BlocProvider(
                create: (context) => StudentAttendanceBloc(studentAttendanceRepository: attendanceRepository),
                child: StudentDetail(student: student,),
              ),
            ),
        ).then((value){
          if(value){
            BlocProvider.of<StudentBloc>(context).add(FetchStudent(classId: this.classroom.id));
          }
        });
      },
      child: Container(
        decoration:   student.checkInTime != "" ? student.checkOutTime != "" ? checkInBothBorder(): checkInMorningBorder() : normalBorder(),
        margin: EdgeInsets.all(20),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Row(

              children: <Widget>[
                student.studentImage != null  && student.studentImage!= "" ? Image.network(
                  "https://anak2u.s3.ap-southeast-1.amazonaws.com/"+student.studentImage,
                  width: 100,
                ) : student.gender == "boy" ? Image.asset('assets/logos/boy.png', width: 100,):
                Image.asset('assets/logos/girl.png', width: 100,)
            ,
            SizedBox(width: 50,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Text(student.studentName, style: TextStyle(fontSize: 30),textAlign: TextAlign.left,),
                SizedBox(height: 10,),
                Row(
                  children: <Widget>[
                    Text("In", style:TextStyle(fontSize: 24),textAlign: TextAlign.left),
                    SizedBox(width: 10,),
                    Icon(Icons.arrow_left),
                    SizedBox(width: 10,),
                    Text(student.checkInTime, style:TextStyle(fontSize: 24)),
                    SizedBox(width: 20,),
                    Text("Out", style:TextStyle(fontSize: 24)),
                    SizedBox(width: 10,),
                    Icon(Icons.arrow_right),
                    SizedBox(width: 10,),
                    Text(student.checkOutTime, style:TextStyle(fontSize: 24))
                  ],
                )
              ],
            )
          ]),
        ),
      ),
    );
  }

  BoxDecoration checkInMorningBorder() {
    return BoxDecoration(
        border: Border.all(

            color: Colors.orange,
            width: 5
        ),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
        )
    );
  }
  BoxDecoration checkInBothBorder() {
    return BoxDecoration(
        border: Border.all(
            color: Colors.green,
            width: 5
        ),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
        )
    );
  }
  BoxDecoration normalBorder() {
    return BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.white10,
            offset: Offset(0.0, 1.0), //(x,y)
            blurRadius: 6.0,
          ),
        ],
        border: Border.all(

            color: Colors.grey,
            width: 1
        ),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
        )
    );
  }
}
