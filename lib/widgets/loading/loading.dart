import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/blocs/user/blocs.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
class LoadingScreen extends StatelessWidget{
  final String loadStr, status;
  LoadingScreen({String loadStr, String status}) : this.loadStr = loadStr, this.status = status;

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.amber[600],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/images/ombreIcon.png", height: 100, width: 100),
              Padding(
                padding: EdgeInsets.all(32),
                child: Container(
                  width: double.maxFinite,
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 32),
                        status != null ? Image.asset(
                          "assets/images/${status}Img.png",
                          fit: BoxFit.scaleDown,
                          height: 60,
                          width: 60,
                        ) : SizedBox(
                          height: 120,
                          width: 120,
                          child: loadIndicator(),
                        ),
                        Padding(
                          padding: EdgeInsets.all(32),
                          child: Text(
                            loadStr,
//                            style: getCustomFont(Color(hexStringToHexInt('#4f4f4f')), 30, 'Lato-Regular'),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        status != null ? LoadingButton() : Container(),
                        SizedBox(height: 32),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

}

class LoadingButton extends StatelessWidget{

  final String status;
  LoadingButton({String status}) : this.status = status;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            if(status == 'success'){
              BlocProvider.of<UserBloc>(context)..add(NavigateTo(page: 'signIn'));
            }else{
              BlocProvider.of<UserBloc>(context)..add(NavigateTo(page: 'back'));
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 32, right: 32),
            height: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              gradient: new LinearGradient(
                // colors: [
                //   Colors.yellow,
                //   Colors.yellowAccent,
                // ],
              ),
            ),
            child: Center(
              child: Text("GO BACK",
                style: TextStyle(color: Colors.white, fontSize: 28),
              ),
            ),
          ),
        )
      ],
    );
  }
}
Widget loadIndicator(){
  return new Stack(
    children: [
      new Opacity(
        opacity: 0.3,
        child: ModalBarrier(dismissible: false, color: Colors.transparent),
      ),
      new Center(
        child: SpinKitThreeBounce(color: Colors.redAccent, size: 40,),
      ),
    ],
  );
}