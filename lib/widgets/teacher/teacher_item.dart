
import 'package:attendanceapp/widgets/teacher/teacherdetail.dart';
import 'package:flutter/material.dart';
import 'package:attendanceapp/models/teacher.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/blocs/teacher_attendance/blocs.dart';
import 'package:attendanceapp/blocs/teacher/blocs.dart';
import 'package:attendanceapp/repositories/teacher_attendance/repositories.dart';
import 'package:http/http.dart' as http;

class TeacherItem extends StatelessWidget {
  final Teacher teacher;
  final int instituteId;
  TeacherItem({this.teacher,this.instituteId});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final TeacherAttendanceRepository attendanceRepository =
        TeacherAttendanceRepository(
          teacherAttendanceClient: TeacherAttendanceClient(
            httpClient: http.Client(),
          ),
        );

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => TeacherAttendanceBloc(
                      teacherAttendanceRepository: attendanceRepository),
                  child: TeacherDetail(teacher: teacher,)
                  ,
                ))).then((value){
                  if (value){
                    BlocProvider.of<TeacherBloc>(context).add(FetchTeacher(instituteId: this.instituteId));
                  }
        })
        ;
      },
      child: Container(
        decoration: teacher.checkInTime != "" ? teacher.checkOutTime != "" ? checkInBothBorder(): checkInMorningBorder() : normalBorder(),
        margin: EdgeInsets.all(20),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Row(

              children: <Widget>[
                teacher.picUrl !="" && teacher.picUrl != null ? Image.network(teacher.picUrl, width: 100, height: 100,) : Image.asset("assets/logos/teachericon.png",width: 100,height: 100),
                SizedBox(width: 50,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Text(teacher.teacherName, style: TextStyle(fontSize: 30),textAlign: TextAlign.left,),
                    SizedBox(height: 10,),
                    Row(
                      children: <Widget>[
                        Text("In", style:TextStyle(fontSize: 24),textAlign: TextAlign.left),
                        SizedBox(width: 10,),
                        Icon(Icons.arrow_left),
                        SizedBox(width: 10,),
                        Text(teacher.checkInTime, style:TextStyle(fontSize: 24)),
                        SizedBox(width: 20,),
                        Text("Out", style:TextStyle(fontSize: 24)),
                        SizedBox(width: 10,),
                        Icon(Icons.arrow_right),
                        SizedBox(width: 10,),
                        Text(teacher.checkOutTime, style:TextStyle(fontSize: 24)),
                      ],
                    )
                  ],
                )
              ]),
        ),
      ),
    );
  }
  BoxDecoration checkInMorningBorder() {
    return BoxDecoration(
      border: Border.all(

          color: Colors.orange,
          width: 5
      ),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
        )
    );
  }
  BoxDecoration checkInBothBorder() {
    return BoxDecoration(
        border: Border.all(
            color: Colors.green,
            width: 5
        ),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
        )
    );
  }
  BoxDecoration normalBorder() {
    return BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.white10,
            offset: Offset(0.0, 1.0), //(x,y)
            blurRadius: 6.0,
          ),
        ],
        border: Border.all(

            color: Colors.grey,
            width: 1
        ),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
        )
    );
  }
}
