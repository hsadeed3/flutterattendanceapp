import 'package:attendanceapp/repositories/teacher_attendance/repositories.dart';
import 'package:attendanceapp/teachertemp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:attendanceapp/widgets/loading/loading.dart';
import 'dart:io';
import 'dart:async';
import 'package:attendanceapp/utils/file_rotate.dart';
import 'package:image_picker/image_picker.dart';
import 'package:attendanceapp/blocs/teacher_attendance/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:attendanceapp/models/teacher.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
class TeacherDetail extends StatefulWidget {
  final Teacher teacher;
  TeacherDetail({this.teacher});
  @override
  _TeacherDetailState createState() => _TeacherDetailState();
}

class _TeacherDetailState extends State<TeacherDetail> {
  String _checkintime = "";
  String _checkoutTime = "";
  String _temperature = "";
  String _conditions = "";
  String _photoCheckIn = "";
  String _photoCheckOut = "";
  int _attendanceId = 0;
  bool hasCheckedIn = false;
  bool hasCheckedOut = false;

  File imageToShow;
  File imageFile;

  _openGallery(BuildContext context) async {
    var picture = await ImagePicker().getImage(source: ImageSource.gallery);
    //library buggy
    //  var imageToTransform = await FlutterExifRotation.rotateImage(path: picture.path);
    this.setState(() {
      imageFile = picture as File;
      imageToShow = picture as File;
    });
//    var imageToTransform = await fixExifRotation(picture);
//    this.setState(()  {
//      imageFile = imageToTransform;
//      imageToShow = imageToTransform;
//    });
    Navigator.of(context).pop();
  }

  _openCamera(BuildContext context) async {
    var picture = await ImagePicker().getImage(source: ImageSource.camera);
    var imageToTransform = await FlutterExifRotation.rotateImage(path: picture.path);
    this.setState(()  {
      imageFile = imageToTransform;
      imageToShow = imageToTransform;
    });
    Navigator.of(context).pop();
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  GestureDetector(
                    child: Text('Gallery'),
                    onTap: () {
                      _openGallery(context);
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    child: Text('Camera'),
                    onTap: () {
                      _openCamera(context);
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  Widget _decideImageView() {
    if (imageFile == null) {
      print('here');
      return Image.asset('assets/logos/takepicture.png',  width: 300,
        height: 300,);
    } else {
      return Image.file(imageToShow );
    }
  }

  @override
  Widget build(BuildContext context) {
    final TeacherAttendanceRepository attendanceRepository =
    TeacherAttendanceRepository(
      teacherAttendanceClient: TeacherAttendanceClient(
        httpClient: http.Client(),
      ),
    );
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.amber[600],
          title: Text(widget.teacher.teacherName),
        ),
        body: BlocProvider<TeacherAttendanceBloc>(
            create: (context) => TeacherAttendanceBloc(
                teacherAttendanceRepository: attendanceRepository),
            child: BlocListener<TeacherAttendanceBloc, TeacherAttendanceState>(
                listener: (context, state) {
                  if (state is TeacherAttendanceSuccess) {
                    Navigator.pop(context,true);
                  }
                }, child:
            BlocBuilder<TeacherAttendanceBloc, TeacherAttendanceState>(
                builder: (context, state) {
                  if (state is TeacherAttendanceLoaded) {
                    final attendanceInfo = state.attendanceInfo;
                    print(attendanceInfo.checkInTime);
                    if (attendanceInfo.checkInTime == null) {

                      DateTime dateTime = DateTime.now();

                      var minute = dateTime.minute.toString().length == 1 ? '0${dateTime.minute}' : '${dateTime.minute}';
                      _checkintime = "${dateTime.hour}:$minute";
                      _checkoutTime = "";
                    } else if (attendanceInfo.checkOutTime == null) {
                      DateTime dateTime = DateTime.now();
                      var minute = dateTime.minute.toString().length == 1 ? '0${dateTime.minute}' : '${dateTime.minute}';
                      _checkintime = attendanceInfo.checkInTime.toString();
                      _checkoutTime = "${dateTime.hour}:$minute";
                      _temperature = attendanceInfo.temperature;
                      _attendanceId = attendanceInfo.attendanceId;
                      _photoCheckIn = attendanceInfo.photoCheckIn;
                      _conditions = attendanceInfo.notes;
                      hasCheckedIn = true;
                    }
                    else {
                      DateTime dateTime = DateTime.now();
                      var minute = dateTime.minute.toString().length == 1 ? '0${dateTime.minute}' : '${dateTime.minute}';
                      _checkintime = attendanceInfo.checkInTime.toString();
                      _checkoutTime = "${dateTime.hour}:$minute";
                      _temperature = attendanceInfo.notes;
                      _attendanceId = attendanceInfo.attendanceId;
                      _photoCheckIn = attendanceInfo.photoCheckIn;
                      _photoCheckOut = attendanceInfo.photoCheckOut;
                      hasCheckedIn = true;
                      hasCheckedOut = true;
                      print(_photoCheckOut);
                    }

                    return SingleChildScrollView(
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () =>  _showChoiceDialog(context),
                              child:  Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  (_photoCheckIn != null && _photoCheckIn != "") ? Expanded(child: Image.network(_photoCheckIn)) : Container(),
                                  (_photoCheckOut != null && _photoCheckOut != "") ? Expanded(child: Image.network(_photoCheckOut)) : Container(),
                                  SizedBox(width: 10,),
                                  !hasCheckedOut ? Expanded(child: _decideImageView()):SizedBox() ,
                                ],
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              color: Colors.amberAccent.withOpacity(0.5),
                              padding: EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text("Check In Time: ", style:TextStyle(fontSize: 24)),
                                      Text(_checkintime,
                                          style: !hasCheckedIn ? TextStyle(fontSize: 24,fontWeight: FontWeight.w800) :TextStyle(fontSize: 24)  ),
                                    ],
                                  ),

                                  SizedBox(height: 16),
                                  Row(
                                      children: <Widget>[
                                        Text("Check Out Time: ", style:TextStyle(fontSize: 24)),
                                        Text(_checkoutTime,
                                            style: hasCheckedIn && !hasCheckedOut ? TextStyle(fontSize: 24,fontWeight: FontWeight.w800) :TextStyle(fontSize: 24)  ),
                                      ]
                                  ),
                                  SizedBox(height: 16),
                                  Text('Temperature:$_temperature',
                                      style: TextStyle(fontSize: 24)),
                                  SizedBox(height: 16),
                                  Text('Conditions:$_conditions',
                                      style: TextStyle(fontSize: 24)),
                                ],
                              ),
                            ),
                            !hasCheckedIn ? SizedBox(
                                width: double.infinity,
                                height: 60,
                                child: RaisedButton(
                                  child: Text('Record Temperature',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white)),
                                  color: Colors.amber[600],
                                  onPressed: () async {
                                    final results = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                TeacherTemperature()));
                                    _temperature = "${results['temp']} C";
                                    _conditions = results["findings"].toString();
                                  },
                                )) : Container(),
                            SizedBox(
                              height: 10,
                            ),
                            !hasCheckedOut? SizedBox(
                                width: double.infinity,
                                height: 60,
                                child: RaisedButton(
                                  child:
                                  !hasCheckedIn ?
                                  Text('Check In',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white)) : Text('Check Out',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white)) ,
                                  color: Colors.amber[600],
                                  onPressed: () {
                                    var dateTime = DateTime.now();
                                    var formattedDate = "${dateTime.year}-${dateTime.month}-${dateTime.day}";
                                    if (!hasCheckedIn) {
                                      if (_temperature == "") {
                                        Fluttertoast.showToast(
                                            msg: "Please fill in temperature to proceed",
                                            toastLength: Toast.LENGTH_SHORT,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0
                                        );
                                      } else {
                                        BlocProvider.of<TeacherAttendanceBloc>(
                                            context)
                                            .add(CreateTeacherAttendance(
                                            teacherId: widget.teacher.teacherId
                                                .toString(),
                                            checkInTime: _checkintime,
                                            temperature: _temperature,
                                            date: formattedDate,
                                            notes: _conditions,
                                            checkInPic: imageFile
                                        ));
                                      }
                                    }
                                    else {
                                      BlocProvider.of<TeacherAttendanceBloc>(context)
                                          .add(SendTeacherAttendance(
                                          attendanceId: _attendanceId,
                                          checkOutTime: _checkoutTime,
                                          photoCheckOut:imageFile
                                      )
                                      );
                                    }

                                  },
                                )):Container()
                          ],
                        ),
                      ),
                    );
                  } else if (state is TeacherAttendanceUninitialized) {
                    BlocProvider.of<TeacherAttendanceBloc>(context)
                        .add(TeacherAttendanceInitialized(teacherId: widget.teacher.teacherId));
                    return LoadingScreen(loadStr:"Please wait");
                  } else if (state is TeacherAttendanceError) {
                    return Container(
                      child: Text("Error"),
                    );
                  }
                  else if (state is TeacherAttendanceLoading){
                    return LoadingScreen(loadStr:"Please wait");
                  }
                  return Container(
                    child: Text("${state.toString()}"),
                  );
                }))));
  }

}
