
import 'package:attendanceapp/blocs/teacher/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'teacher_item.dart';
import 'package:attendanceapp/models/user.dart';

class Teacher extends StatelessWidget {
  final int instituteId ;
  Teacher({this.instituteId});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.amber[600],
          title: Text('Teacher List'),
        ),
        body:
        Center(

          child: BlocBuilder<TeacherBloc, TeacherState>(
            builder: (context, state) {
              if (state is TeacherEmpty) {

                print('load');
                BlocProvider.of<TeacherBloc>(context).add(FetchTeacher(instituteId: this.instituteId));
                return Center(child: Text('Please Wait'));
              } else if (state is TeacherLoading) {
                return Center(child: CircularProgressIndicator());
              } else if (state is TeacherLoaded) {
                final teachers = state.teachers;

                return ListView.builder(
                  itemCount: teachers.length,
                  itemBuilder: (context, position) {
                    return TeacherItem(teacher:teachers[position],instituteId: instituteId,);
                  },
                );
              } else {
                return Text(
                  'Something went wrong!',
                  style: TextStyle(color: Colors.red),
                );
              }
            },
          ),
        ));
  }

}

