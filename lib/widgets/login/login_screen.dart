import 'dart:async';

import 'package:attendanceapp/models/user.dart';
import 'package:attendanceapp/repositories/user/repositories.dart';
import 'package:attendanceapp/widgets/forgotpassword.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../utils/blecontroller.dart';
import 'signin_form.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/blocs/user/blocs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../select_user.dart';
import 'package:http/http.dart' as http;
import 'package:attendanceapp/widgets/loading/loading.dart';
import 'package:attendanceapp/studenttemp.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final BleController bleCtrl = Get.put(BleController(""));

  @override
  void initState() {
    super.initState();
  Timer.periodic(Duration(seconds: 5),(Timer t)=>bleCtrl.connect());



    ///whatever you want to run on page build
  }

  @override
  Widget build(BuildContext context) {
    final UserRepository userRepository = UserRepository(
      userApiClient: UserApiClient(
        httpClient: http.Client(),
      ),
    );
    return Scaffold(
      body:  BlocProvider<UserBloc>(
        create: (context) => UserBloc(userRepository:userRepository),
        child: BlocListener<UserBloc, UserState>(
          listener: (context, state){
            if(state is Authenticated) {

              print(state.user.id);
              print(state.user.name);
              print('token');
              print(state.user.token);

              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (BuildContext context) => SelectUser(user:state.user)),


              );
            }
          }
          ,
          child: BlocBuilder<UserBloc, UserState>(
              builder: (context, state) {
                if(state is Uninitialized){
                    BlocProvider.of<UserBloc>(context).add(CheckLogin());
                    return Center(child: Text('Please Wait'));
                }
                if (state is NotLoggedIn){
                  return SignInForm();
                }
                if(state is Loading){
                  return LoadingScreen(loadStr: state.msg.toString());
                }
                if(state is AuthenticatedError){
                  return LoadingScreen(loadStr: state.error.toString(), status: 'failed',);
                }
                if(state is NavigateToPage){
                  if(state.page == 'back'){
                    return SignInForm();
                  }
                }
                return Container();
              }
          ),
        ),
      )

    );
    }
}
/*,*/