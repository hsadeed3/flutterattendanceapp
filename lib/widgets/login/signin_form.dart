import 'package:flutter/material.dart';
import 'package:attendanceapp/widgets/select_user.dart';
import 'package:attendanceapp/blocs/user/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/widgets/forgotpassword.dart';
String email = '';
String password = '';

class SignInForm extends StatefulWidget {
  @override
  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {

  final _signInFormKey = GlobalKey<FormState>();

  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _signInFormKey,
      child:
         Stack(
           fit:StackFit.expand,
           children: <Widget>[
             Container(
               color: Colors.amber[600],
             ),
             Center(
               child: SingleChildScrollView(
                 child: Container(
                   color: Colors.transparent,
                   child: Padding(
                     padding: const EdgeInsets.all(60.0),
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         Image.asset('assets/logos/anak2u.png'),
                         Text(
                           'Attendance Application',
                           style: TextStyle(
                               color: Colors.white,
                               fontFamily: 'OpenSans',
                               fontSize: 25.0),
                         ),
                         SizedBox(height: 5),
                         Image.asset('assets/logos/logo.png', width: 250, height: 250
                         ),
                         SizedBox(height: 20),
                         _buildEmailTF(),
                         _buildPasswordTF(),
                         _buildLoginBtn(),
                         SizedBox(height: 10),
                         Container(
                           child: Column(
                             children: <Widget>[

                               _buildInviteBtn(),
                               SizedBox(height: 5),
                               _buildForgotPasswordBtn(),

                             ],
                           ),
                         )
                       ],
                     ),
                   ),
                 ),
               ),
             ),
           ],

         ),


    );
  }

  FocusNode nodeEmail = FocusNode();
  FocusNode nodePassword = FocusNode();

  @override
  void initState() {
    super.initState();
    emailController = new TextEditingController(text: email);
    passwordController = new TextEditingController(text: password);
  }


  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          alignment: Alignment.centerLeft,
          color: Colors.white.withOpacity(0.7),
          height: 50.0,
          width: 300.0,
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            controller: emailController,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.amberAccent,
              ),
              hintText: 'Enter your Email',
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          color: Colors.white.withOpacity(0.7),
          height: 50.0,
          width: 300.0,
          child: TextField(
            obscureText: true,
            controller: passwordController,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.amberAccent,
              ),
              hintText: 'Enter your Password',
            ),
          ),
        ),
      ],
    );
  }
  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () {
          _onButtonPressed();
        },
        padding: EdgeInsets.all(8.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.amber[800],
        child: Text(
          'Log In',
          style: TextStyle(
            color: Color(0xFFFFFFFF),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }
  
  _onButtonPressed(){
    if(_signInFormKey.currentState.validate()){
      email = emailController.text;
      password = passwordController.text;
      BlocProvider.of<UserBloc>(context).add(SignInUser(email: email, password: password));
    }
  }
}
Widget _buildForgotPasswordBtn() {
  return GestureDetector(
    onTap: () {
//      Navigator.push(
//          context, MaterialPageRoute(builder: (context) => ForgotPassword()));
    },
    child: RichText(
      text: TextSpan(children: [
        TextSpan(
            text: ('Forgot Password'),
            style: TextStyle(color: Colors.white, fontSize: 18.0))
      ]),
    ),
  );
}


Widget _buildInviteBtn() {
  return GestureDetector(
    onTap: () {
//      Navigator.push(
//          context, MaterialPageRoute(builder: (context) => InviteSchool()));
    },
    child: RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: 'Invite Your School',
            style: TextStyle(color: Colors.white, fontSize: 18.0),
          ),
        ],
      ),
    ),
  );
}

Widget _buildSignupBtn() {
  return GestureDetector(
    onTap: () => print('Sign Up Button Pressed'),
    child: RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: 'Free Trial Access',
            style: TextStyle(color: Colors.white, fontSize: 18.0),
          ),
        ],
      ),
    ),
  );
}