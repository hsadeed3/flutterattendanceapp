import 'package:flutter/material.dart';

class InviteSchool extends StatefulWidget {
  @override
  _InviteSchoolState createState() => _InviteSchoolState();
}

class _InviteSchoolState extends State<InviteSchool> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text('Invite Your school'),
        backgroundColor: Colors.amber[600],
      ),
      body: SingleChildScrollView(
        child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('School Name:', style: TextStyle(fontSize: 18)),
                  TextField(keyboardType: TextInputType.text),
                  SizedBox(height: 10),
                  Text('School E-Mail:', style: TextStyle(fontSize: 18)),
                  TextField(keyboardType: TextInputType.emailAddress),
                  SizedBox(height: 10),
                  Text('School Phone Number:', style: TextStyle(fontSize: 18)),
                  TextField(keyboardType: TextInputType.number),
                  SizedBox(height: 10),
                  Text('Your Name:', style: TextStyle(fontSize: 18)),
                  TextField(keyboardType: TextInputType.text),
                  SizedBox(height: 10),
                  Text('Your E-Mail:', style: TextStyle(fontSize: 18)),
                  TextField(keyboardType: TextInputType.emailAddress),
                  SizedBox(height: 20),
                  RaisedButton(
                    color: Colors.amber[600],
                    child: Text('Submit',style: TextStyle(fontSize: 18)),
                    textColor: Colors.white,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            )),
      ),
    );
  }
}
