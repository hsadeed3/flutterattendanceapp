import 'package:attendanceapp/blocs/student/blocs.dart';
import 'package:attendanceapp/repositories/student/repositories.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/models/classroom.dart';
import 'package:attendanceapp/widgets/student/student.dart';
class ClassItem extends StatelessWidget {
  final Classroom classroom ;
  ClassItem({this.classroom});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        final VisitorRepository studentRepository = VisitorRepository(
          visitorApiClient: StudentApiClient(
            httpClient: http.Client(),
          ),
        );
        Navigator.push(context,
            MaterialPageRoute(builder: (context) =>
                BlocProvider(
                  create: (context) => StudentBloc(studentRepository:studentRepository),
                  child: Student(classroom: classroom),
                )
            )
        );
      },
      child: Card(
          margin: EdgeInsets.all(20),
      child: Padding(
      padding: const EdgeInsets.all(20.0),
      child: Text(classroom.classroomName,style: TextStyle(fontSize: 30))
      )
      ),
    );
  }
}
