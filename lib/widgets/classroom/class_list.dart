import 'package:attendanceapp/blocs/classroom/blocs.dart';

import 'class_item.dart';
import 'package:attendanceapp/blocs/teacher/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:attendanceapp/models/user.dart';
class ClassList extends StatelessWidget {
  final User user ;
  ClassList({this.user});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.amber[600],
          title: Text('Class List'),
        ),
        body: Center(
          child: BlocBuilder<ClassroomBloc, ClassroomState>(
            builder: (context, state) {
              if (state is ClassroomEmpty) {
                print('load');
                BlocProvider.of<ClassroomBloc>(context)
                    .add(FetchClassroom(schoolId: this.user.id));
                return Center(child: Text('Please Wait'));
              } else if (state is ClassroomLoading) {
                return Center(child: CircularProgressIndicator());
              } else if (state is ClassroomLoaded) {
                final classrooms = state.classrooms;
                return ListView.builder(
                  itemCount: classrooms.length,
                  itemBuilder: (context, position) {
                    return ClassItem(classroom: classrooms[position]);
                  },
                );
              } else {
                return Text(
                  'Something went wrong!',
                  style: TextStyle(color: Colors.red),
                );
              }
            },
          ),
        ));
  }
}
