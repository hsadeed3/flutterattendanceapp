import 'package:attendanceapp/QrCodeScanner.dart';
import 'package:attendanceapp/blocs/teacher/blocs.dart';
import 'package:attendanceapp/blocs/visitor/blocs.dart';
import 'package:attendanceapp/repositories/teacher/repositories.dart';
import 'package:attendanceapp/repositories/classroom/repositories.dart';
import 'package:attendanceapp/blocs/classroom/blocs.dart';
import 'package:attendanceapp/blocs/visitor/blocs.dart';
import 'package:attendanceapp/repositories/visitor/repositories.dart';
import 'package:attendanceapp/widgets/visitor/visitor.dart';
import 'package:attendanceapp/widgets/classroom/class_list.dart';
import 'package:attendanceapp/widgets/login/login_screen.dart';
import 'package:attendanceapp/widgets/classroom/class_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:attendanceapp/widgets/teacher/teacher.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:attendanceapp/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../RegistertThermometer.dart';
class SelectUser extends StatelessWidget {
  final User user;
  SelectUser({this.user});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.amber[600],
        title: Text(this.user.name),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.exit_to_app),
            color: Colors.white,
            onPressed: () {
              createAlertDialog(context);
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      final TeacherRepository teacherRepository =
                          TeacherRepository(
                        teacherApiClient: TeacherApiClient(
                          httpClient: http.Client(),
                        ),
                      );
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                    create: (context) => TeacherBloc(
                                        teacherRepository: teacherRepository),
                                    child: Teacher(instituteId: this.user.id),
                                  )));
                    },
                    child: Container(
                      child: Image.asset('assets/logos/teacherbutton.png'),
                    ),
                  ),
                ),
                SizedBox(width: 20,),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      final ClassroomRepository classroomRepository =
                          ClassroomRepository(
                        classroomApiClient: ClassroomApiClient(
                          httpClient: http.Client(),
                        ),
                      );
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                    create: (context) => ClassroomBloc(
                                        classroomRepository: classroomRepository),
                                    child: ClassList(user: this.user),
                                  )));
                    },
                    child: Image.asset('assets/logos/studentbutton.png'),
                  ),
                ),
                SizedBox(width: 20,),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      final ClassroomRepository classroomRepository =
                      ClassroomRepository(
                        classroomApiClient: ClassroomApiClient(
                          httpClient: http.Client(),
                        ),
                      );
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                create: (context) => ClassroomBloc(
                                    classroomRepository: classroomRepository),
                                child: ClassList(user: this.user),
                              )));
                    },
                    child: Image.asset('assets/logos/studentbutton.png'),
                  ),
                )
              ],

            ),
            SizedBox(height: 20,),
            GestureDetector(
              child: Image.asset('assets/logos/visitor.png'),
              onTap: (){
                final VisitorRepository visitorRepository =
                VisitorRepository(
                  visitorApiClient: VisitorApiClient(
                    httpClient: http.Client(),
                  ),
                );
                Navigator.push(context, MaterialPageRoute(builder: (context)=>BlocProvider(
                  create:(context) => VisitorBloc(
                    visitorRepository:visitorRepository
                  ),
                  child: Visitor(),
                )));
              },
            ),

            SizedBox(height: 20,),
            GestureDetector(
              child: Image.asset('assets/logos/visitor.png'),
              onTap: (){
                final VisitorRepository visitorRepository =
                VisitorRepository(
                  visitorApiClient: VisitorApiClient(
                    httpClient: http.Client(),
                  ),
                );
                Navigator.push(context, MaterialPageRoute(builder: (context)=>InitiateRegistration(),


                ));
              },
            ),

            SizedBox(height: 20,),
            GestureDetector(
              child: Image.asset('assets/logos/visitor.png'),
              onTap: (){
                final VisitorRepository visitorRepository =
                VisitorRepository(
                  visitorApiClient: VisitorApiClient(
                    httpClient: http.Client(),
                  ),
                );
                Navigator.push(context, MaterialPageRoute(builder: (context)=>QrCodeScanner(),


                ));
              },
            )
          ],
        ),
      ),
    );
  }
}

createAlertDialog(BuildContext context) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Log Out'),
          content: Text('Are you sure you want to log out?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes'),
              textColor: Colors.white,
              color: Colors.teal,
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.remove("user");

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                );
              },
            ),
            FlatButton(
              child: Text('No'),
              textColor: Colors.white,
              color: Colors.pink[300],
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
          elevation: 24.0,
        );
      });
}
