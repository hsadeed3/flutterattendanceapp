import 'package:attendanceapp/blocs/visitor/blocs.dart';
import 'package:attendanceapp/models/state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Visitor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
          title: Text('New Visitor Info'), backgroundColor: Colors.amber[600]),
      body: SingleChildScrollView(child: VisitorForm()),
    );
  }
}

class VisitorForm extends StatefulWidget {
  @override
  _VisitorFormState createState() => _VisitorFormState();
}

class _VisitorFormState extends State<VisitorForm> {
  final _formKey = GlobalKey<FormState>();
  String name = "";
  String address = "";
  String purpose = "";
  String temperature = "";
  String contact = "";

  @override
  Widget build(BuildContext context) {
    return
      BlocListener<VisitorBloc, VisitorState>(
          listener: (context, state) {
            if (state is VisitorSuccess) {
              Navigator.pop(context);
            }
          }, child: BlocBuilder<VisitorBloc, VisitorState>(
          builder: (context, state) {
            if (state is VisitorInitialized) {
              return Form(
                onChanged: () {
                  Form.of(primaryFocus.context).save();
                },
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(50.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                          onSaved: (String value) {
                            this.name = value;
                          },
                          decoration: InputDecoration(
                              labelText: 'Name',
                              filled: true,
                              fillColor: Colors.white),
                          style: TextStyle(fontSize: 25),
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Please enter visitor name';
                            }
                            return null;
                          }),
                      SizedBox(height: 10),
                      TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Contact Number',
                              filled: true,
                              fillColor: Colors.white),
                          style: TextStyle(fontSize: 25),
                          onSaved: (String value) {
                            this.contact = value;
                          },
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Please enter contact number';
                            }
                            return null;
                          }),
                      SizedBox(height: 10),
                      TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Address',
                              filled: true,
                              fillColor: Colors.white),
                          style: TextStyle(fontSize: 25),
                          onSaved: (String value) {
                            this.address = value;
                          },
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Please enter Address';
                            }
                            return null;
                          }),
                      SizedBox(height: 10),
                      TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Purpose of Visit',
                              filled: true,
                              fillColor: Colors.white),
                          style: TextStyle(fontSize: 25),
                          onSaved: (String value) {
                            this.purpose = value;
                          },
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Please enter purpose of visit';
                            }
                            return null;
                          }),
                      SizedBox(height: 10),
                      TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Temperature',
                              filled: true,
                              fillColor: Colors.white),
                          style: TextStyle(fontSize: 25),
                          onSaved: (String value) {
                            this.temperature = value;
                          },
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Please enter temperature';
                            }
                            return null;
                          }),
                      SizedBox(height: 25),
                      Row(
                        children: <Widget>[
                          FlatButton(
                              child: Text('Cancel',
                                  style: TextStyle(
                                      color: Colors.lightBlueAccent,
                                      fontSize: 30)),
                              color: Colors.white,
                              padding: EdgeInsets.all(10),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(100)),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          SizedBox(width: 25),
                          FlatButton(
                            child: Text('Submit',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 30)),
                            color: Colors.lightBlueAccent,
                            padding: EdgeInsets.all(10),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100)),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                BlocProvider.of<VisitorBloc>(context).add(
                                    SendVisitorAttendance(name: name,
                                        address: address,
                                        contact: contact,
                                        purpose: purpose,
                                        temperature: temperature));
                              }
                            },
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }
            if (state is VisitorLoading) {
              return Container(
                child: Text("Loading"),
              );
            }
            if (state is SuccessState) {
              // set up the button
              Widget okButton = FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.pop(context);
                },
              );

              // set up the AlertDialog
              AlertDialog alert = AlertDialog(
                title: Text("Berjaya"),
                content: Text("Pendaftaran pelawat berjaya"),
                actions: [
                  okButton,
                ],
              );

              // show the dialog
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return alert;
                },
              );
            //  Navigator.pop(context);
            }
            if (state is VisitorError) {
              return Container(
                child: Text("Something went wrong"),
              );
            }

            return Container();
          }
      )
      );
  }

}