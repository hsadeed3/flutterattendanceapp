import 'dart:developer';




import 'package:attendanceapp/QrCodeScanner.dart';
import 'package:attendanceapp/utils/blecontroller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:attendanceapp/models/bleModels.dart';
import 'package:attendanceapp/commons.dart';
import 'DeviceScanner.dart';
import 'appData.dart';

SharedPreferences prefs;





class ThermoRegistration extends StatelessWidget {
  bleModels BleModels=bleModels();
  static Future init() async {
    prefs = await SharedPreferences.getInstance();
    String firstName = prefs.getString('Default');

  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: InitiateRegistration(),
    );
  }
}

class InitiateRegistration extends StatefulWidget {
  @override
  State<InitiateRegistration> createState() => _InitiateRegistrationState();

}

class _InitiateRegistrationState extends State<InitiateRegistration> {
  final TextEditingController _MacIDControllerText=TextEditingController();

  @override
  Widget build(BuildContext context) {
    macIdSave=_MacIDControllerText.text.toString();
    return Scaffold(

      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
padding: EdgeInsets.all(10),
                child:  TextField(

                  obscureText: false,
                  controller: _MacIDControllerText,

                  decoration: InputDecoration(
                    labelText:'Please key in the physical address of your device',
                    border: OutlineInputBorder(),

                  ),


                ),

              ),
              Text(
                "Digital Thermometer",
                style: TextStyle(
                    fontSize: 15,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 50,
              ),
              RaisedButton(child: Text("Register your device"),

                  onPressed:(){
setState(() {
macIdSave;
});
                save();
              }),
              SizedBox(
                height: 50,
              ),

              RaisedButton(child: Text("Remove Device"),
                  onPressed:(){
                 remove();


                   Fluttertoast.showToast(
                       msg: "Device removed successfully ",
                       toastLength: Toast.LENGTH_SHORT,
                       timeInSecForIosWeb: 1,
                       backgroundColor: Colors.red,
                       textColor: Colors.white,
                       fontSize: 16.0);

                  }),
              RaisedButton(child: Text("Mac Explorer"), onPressed:(){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>FlutterBlueApp()),
               );
              })
            ],
          ),
        ),
      ),
    );
  }
}

save() async {
  await ThermoRegistration.init();

  prefs.setString('string', macIdSave);

  if (macIdSave.isNotEmpty) {
    Fluttertoast.showToast(
        msg: "The device with physical address ${macIdSave} has been configured. ",
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }
  else{
    Fluttertoast.showToast(
        msg: "Please key in your device address",
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}

 fetch() async {



 String Stringval = prefs.getString('string') ?? '';




  log(Stringval);




}


remove() async {
  SharedPreferences prefs=await SharedPreferences.getInstance();
await prefs.clear();
}