import 'package:attendanceapp/CheckInData.dart';
import 'package:attendanceapp/blocs/student/blocs.dart';
import 'package:attendanceapp/fetchingdetail.dart';
import 'package:attendanceapp/healthreport.dart';
import 'package:attendanceapp/moreinfo.dart';
import 'package:attendanceapp/repositories/student/repositories.dart';
import 'package:attendanceapp/repositories/student_attendance/repositories.dart';
import 'package:attendanceapp/studenttemp.dart';
import 'package:attendanceapp/utils/observe_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'package:attendanceapp/widgets/loading/loading.dart';
import 'package:attendanceapp/models/parent.dart';
import 'package:image_picker/image_picker.dart';
import 'package:attendanceapp/widgets/studentnote.dart';
import 'package:attendanceapp/blocs/student_attendance/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:attendanceapp/models/student.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:attendanceapp/utils/file_rotate.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../tempUIPage.dart';
import 'models/user.dart';

Map studentData = Map();

class UserCheckIn extends StatefulWidget {

  final User user;
UserCheckIn({this.user});


  @override
  _UserCheckInState createState() => _UserCheckInState();
}

class _UserCheckInState extends State<UserCheckIn> implements StateListener{
  String _checkintime = "";
  String _checkoutTime = "";
  String _temperature = "";
  String _temperatureout = "";
  String _conditions = "";
  Parent _selectedParent;
  String _photoCheckIn = "";
  String _photoCheckOut = "";
  int _attendanceId = 0;
  bool hasCheckedIn = false;
  bool hasCheckedOut = false;
  File imageToShow;
  File imageFile;

  _openGallery(BuildContext context) async {
    var picture = await ImagePicker().getImage(source: ImageSource.gallery);
    //library buggy
    //  var imageToTransform = await FlutterExifRotation.rotateImage(path: picture.path);
    this.setState(() {
      imageFile = picture as File;
      imageToShow = picture as File;
    });
//    var imageToTransform = await fixExifRotation(picture);
//    this.setState(()  {
//      imageFile = imageToTransform;
//      imageToShow = imageToTransform;
//    });
    Navigator.of(context).pop();
  }

  _openCamera(BuildContext context) async {
    var picture = await ImagePicker().getImage(source: ImageSource.camera);
    var imageToTransform =
    await FlutterExifRotation.rotateImage(path: picture.path);
    this.setState(() {
      imageFile = imageToTransform;
      imageToShow = imageToTransform;
    });
    Navigator.of(context).pop();
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  GestureDetector(
                    child: Text('Gallery'),
                    onTap: () {
                      _openGallery(context);
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    child: Text('Camera'),
                    onTap: () {
                      _openCamera(context);
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  Widget _decideImageView() {
    if (imageFile == null) {
      print('here');
      return Expanded(
        child: Image.asset(
          'assets/logos/takepicture.png',
          width: 250,
          height: 250,
        ),
      );
    } else {
      return Expanded(child: Image.file(imageToShow));
    }
  }

  Widget notesCard(context) {
    return Hero(
      tag: 'Notes',
      transitionOnUserGestures: true,
      child: Card(
        color: Colors.amber[600],
        child: InkWell(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10.0),
                child: Row(
                  children: <Widget>[
                    Text('',
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Row(
//                  children: <Widget>[
//                    Text('Written Notes',
//                        style: TextStyle(color: Colors.white, fontSize: 20)),
//                  ],
//                ),
//              )
            ],
          ),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => EditNotesView()));
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    studentData = Map();
  }

  @override
  Widget build(BuildContext context) {
    final StudentAttendanceRepository attendanceRepository =
    StudentAttendanceRepository(
      studentAttendanceClient: StudentAttendanceClient(
        httpClient: http.Client(),
      ),
    );
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.amber[600],
          title: Text("Hi"),
        ),
        body: BlocProvider<StudentAttendanceBloc>(
            create: (context) => StudentAttendanceBloc(
                studentAttendanceRepository: attendanceRepository),
            child: BlocListener<StudentAttendanceBloc, StudentAttendanceState>(
                listener: (context, state) {
                  if (state is StudentAttendanceSuccess) {
                    Navigator.pop(context, true);
                  }
                }, child:
            BlocBuilder<StudentAttendanceBloc, StudentAttendanceState>(
                builder: (context, state) {
                  if (state is StudentAttendanceLoaded) {
                    final attendanceInfo = state.attendanceInfo;
                    print(attendanceInfo);

                    if (attendanceInfo.checkInTime == null ||
                        attendanceInfo.checkInTime == "") {
                      DateTime dateTime = DateTime.now();

                      var minute = dateTime.minute.toString().length == 1
                          ? '0${dateTime.minute}'
                          : '${dateTime.minute}';
                      _checkintime = "${dateTime.hour}:$minute";
                      _checkoutTime = "";
                    } else if (attendanceInfo.checkOutTime == null ||
                        attendanceInfo.checkOutTime == "") {
                      DateTime dateTime = DateTime.now();
                      var minute = dateTime.minute.toString().length == 1
                          ? '0${dateTime.minute}'
                          : '${dateTime.minute}';
                      _checkintime = attendanceInfo.checkInTime.toString();
                      _checkoutTime = "${dateTime.hour}:$minute";
                      _temperature = attendanceInfo.temperature;
                      _attendanceId = attendanceInfo.attendanceId;
                      _photoCheckIn = attendanceInfo.photoCheckIn;
                      _conditions = attendanceInfo.notes;
                      hasCheckedIn = true;
                    } else {
                      DateTime dateTime = DateTime.now();
                      var minute = dateTime.minute.toString().length == 1
                          ? '0${dateTime.minute}'
                          : '${dateTime.minute}';
                      _checkintime = attendanceInfo.checkInTime.toString();
                      _checkoutTime = "${dateTime.hour}:$minute";
                      _temperature = attendanceInfo.notes;
                      _attendanceId = attendanceInfo.attendanceId;
                      _photoCheckIn = attendanceInfo.photoCheckIn;
                      _photoCheckOut = attendanceInfo.photoCheckOut;
                      hasCheckedIn = true;
                      hasCheckedOut = true;
                    }
                    return SingleChildScrollView(
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () => _showChoiceDialog(context),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  (_photoCheckIn != null && _photoCheckIn != "")
                                      ? Expanded(
                                      child: Image.network(_photoCheckIn))
                                      : Container(),
                                  (_photoCheckOut != null && _photoCheckOut != "")
                                      ? Expanded(
                                      child: Image.network(_photoCheckOut))
                                      : Container(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  !hasCheckedOut ? _decideImageView() : SizedBox(),
                                ],
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              color: Colors.amberAccent.withOpacity(0.5),
                              padding: EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text("Check In Time: ",
                                          style: TextStyle(fontSize: 24)),
                                      Text(_checkintime,
                                          style: !hasCheckedIn
                                              ? TextStyle(
                                              fontSize: 24,
                                              fontWeight: FontWeight.w800)
                                              : TextStyle(fontSize: 24)),
                                    ],
                                  ),
                                  SizedBox(height: 16),
                                  Row(children: <Widget>[
                                    Text("Check Out Time: ",
                                        style: TextStyle(fontSize: 24)),
                                    Text(_checkoutTime,
                                        style: hasCheckedIn && !hasCheckedOut
                                            ? TextStyle(
                                            fontSize: 24,
                                            fontWeight: FontWeight.w800)
                                            : TextStyle(fontSize: 24)),
                                  ]),
                                  SizedBox(height: 16),
                                  Text('Check In Temperature:$_temperature',
                                      style: TextStyle(fontSize: 24)),
                                  SizedBox(height: 16),
                                  Text('Check Out Temperature:$_temperatureout',
                                      style: TextStyle(fontSize: 24)),
                                  SizedBox(height: 16),
                                  Text('Conditions:$_conditions',
                                      style: TextStyle(fontSize: 24)),
                                  SizedBox(height: 16),
                                  Row(
                                    children: <Widget>[
                                      Text('Sender: ',
                                          style: TextStyle(fontSize: 24)),
                                      Text(
                                          _selectedParent != null
                                              ? _selectedParent.parentName
                                              : "",
                                          style: TextStyle(fontSize: 24))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            !hasCheckedOut
                                ? SizedBox(
                                width: double.infinity,
                                height: 60,
                                child: RaisedButton(
                                  child: !hasCheckedIn
                                      ? Text('Record Temperature',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white))
                                      : Text('Record Temperature',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white)),
                                  color: Colors.amber[600],
                                  onPressed: () async {
                                    final results = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                              CheckInData()));


                                    // setState(() {
                                    //   attendanceInfo=results;
                                    // });
                                  },
                                ))
                                : Container(),
                            SizedBox(height: 10),
                            SizedBox(
                                width: double.infinity,
                                height: 60,
                                child: RaisedButton(
                                  child: !hasCheckedIn
                                      ? Text('Digital Thermometer',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white))
                                      : Text('Digital Thermometer',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white)),
                                  color: Colors.amber[600],
                                  onPressed: () async {
                                    final results = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                tempUIPage()));

                                    print(results);
                                    // setState(() {
                                    //   attendanceInfo=results;
                                    // });
                                  },
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            !hasCheckedOut
                                ? SizedBox(
                                width: double.infinity,
                                height: 60,
                                child: RaisedButton(
                                  child: !hasCheckedIn
                                      ? Text('Check In',
                                      style: TextStyle(
                                          fontSize: 30,
                                          color: Colors.white))
                                      : Text('Check Out',
                                      style: TextStyle(
                                          fontSize: 30,
                                          color: Colors.white)),
                                  color: Colors.amber[600],
                                  onPressed: () {
                                    var dateTime = DateTime.now();
                                    var formattedDate =
                                        "${dateTime.year}-${dateTime.month}-${dateTime.day}";
                                    if (!hasCheckedIn) {
                                      if (_temperature == "") {
                                        Fluttertoast.showToast(
                                            msg:
                                            "Please fill in temperature to proceed",
                                            toastLength: Toast.LENGTH_SHORT,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      } else if (_selectedParent == null) {
                                        Fluttertoast.showToast(
                                            msg:
                                            "Please fill in sender to proceed",
                                            toastLength: Toast.LENGTH_SHORT,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      } else {
                                        BlocProvider.of<StudentAttendanceBloc>(
                                            context)
                                            .add(CreateStudentAttendance(
                                            studentId:
                                            widget.user.id,
                                            checkInTime: _checkintime,
                                            temperature: _temperature,
                                            date: formattedDate,
                                            notes: _conditions,
                                            sender: _selectedParent.parentId
                                                .toString(),
                                            photoCheckIn: imageFile));
                                      }
                                    } else {
                                      if (_temperature == "") {
                                        Fluttertoast.showToast(
                                            msg:
                                            "Please fill in temperature to proceed",
                                            toastLength: Toast.LENGTH_SHORT,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      } else if (_selectedParent == null) {
                                        Fluttertoast.showToast(
                                            msg:
                                            "Please fill in sender to proceed",
                                            toastLength: Toast.LENGTH_SHORT,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      } else {
                                        BlocProvider.of<StudentAttendanceBloc>(
                                            context)
                                            .add(UpdateStudentAttendance(
                                            attendanceId: _attendanceId,
                                            checkOutTime: _checkoutTime,
                                            checkOutPic: imageFile,
                                            temperature: _temperatureout,
                                            parent: _selectedParent.parentId
                                                .toString()));
                                      }
                                    }
                                  },
                                ))
                                : Container(),
                            SizedBox(height: 10),
                          ],
                        ),
                      ),
                    );
                  } else if (state is StudentAttendanceUninitialized) {
                    BlocProvider.of<StudentAttendanceBloc>(context).add(
                        StudentAttendanceInitialized(
                            studentId: widget.user.id));
                    return LoadingScreen(loadStr: "Please wait");
                  } else if (state is StudentAttendanceError) {
                    return Container(
                      child: Text("Error"),
                    );
                  } else if (state is StudentAttendanceLoading) {
                    return LoadingScreen(loadStr: "Please wait");
                  }
                  return Container(
                    child: Text("${state.toString()}"),
                  );
                }))));
  }

  _UserCheckInState(){
    var stateProvider = new StateProvider();
    stateProvider.subscribe(this);
  }

  @override
  void dispose() {
    var stateProvider = new StateProvider();
    stateProvider.dispose(this);
    super.dispose();
  }
  @override
  void onStateChanged(ObserverState state) {
    if(state == ObserverState.RELOAD_STUDENT_RECORD){
      _selectedParent = studentData['parent'];
      print(hasCheckedIn);
      print(studentData);
      if (!hasCheckedIn){
        _temperature = "${studentData['temp']} C";
      }
      else {
        _temperatureout="${studentData['temp']} C";
      }

      _conditions = studentData["findings"].toString();
      setState(() {});
    }
  }
}
