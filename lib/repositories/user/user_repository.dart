import 'dart:io';
import 'package:flutter/material.dart';
import 'user_api_client.dart';
import 'package:attendanceapp/models/state.dart';
class UserRepository {
  final UserApiClient userApiClient;

  UserRepository({@required this.userApiClient})
      : assert(userApiClient != null);

  Future<StateModel> loginUser(String email, String password) async =>
      await userApiClient.loginViaEmail(email, password);
}