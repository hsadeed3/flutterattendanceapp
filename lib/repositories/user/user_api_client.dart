import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:attendanceapp/models/state.dart';
import 'package:attendanceapp/models/user.dart';
class UserApiClient {
  final http.Client httpClient;
  static const baseUrl = 'https://dashboard.anak2u.com.my/api/v2';

  UserApiClient({@required this.httpClient}) : assert(httpClient != null);
  Future<StateModel> loginViaEmail(String email, String password) async{

   // if(await checkConnection()){
    //To change to adminLogin
      final loginUrl = '$baseUrl/adminlogin';
      final response = await this.httpClient.post(
        Uri.parse(loginUrl),
        body: {
          'email': email,
          'password': password,
        },
      );

      if(response.statusCode >= 500) return StateModel<String>.error("Sorry to  disappoint you, but something went wrong.");

      final responseBody = jsonDecode(response.body);

      if (responseBody["response"] == "error") {
          return StateModel<String>.error('${responseBody["message"]}');
      }

      User user = User.fromJson(responseBody["result"]);
      print(responseBody["result"]);
      return StateModel<User>.success(user);
//    }else{
//      return StateModel<String>.error(errorInternetMsg);
//    }
  }
}