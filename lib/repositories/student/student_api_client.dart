import 'package:attendanceapp/models/attendance_info.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:attendanceapp/models/parent.dart';
import 'package:attendanceapp/models/student.dart';

class StudentApiClient {
  static const baseUrl = 'https://dashboard.anak2u.com.my/';
  final http.Client httpClient;

  StudentApiClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Student>> getStudentsByClass(int classId) async {
    final studentUrl = '$baseUrl/api/class/$classId/students';
    print('classID');
    print(classId);
    print(studentUrl);
    final studentResponse = await this.httpClient.get(Uri.parse(studentUrl));

    if (studentResponse.statusCode != 200) {
      throw Exception('error getting students for class');
    }

    List<Student> list = Student.listFromJson(json.decode(studentResponse.body));
    return list;
  }
  Future<List<Student>> getTodayClassroomAttendance(int classId) async {
    final classroomUrl = '$baseUrl/api/v2/class-today-attendance/$classId';
    print(classroomUrl);
    final classroomResponse = await this.httpClient.get(Uri.parse(classroomUrl));
    print(classroomResponse.body);

    if (classroomResponse.statusCode != 200) {
      throw Exception('error getting students for class');
    }

    List<Student> list = Student.listFromJson(json.decode(classroomResponse.body));
    return list;
  }

  Future<List<Parent>> getStudentParent(int studentId) async {
    final classroomUrl = '$baseUrl/api/v2/student-parent/$studentId';
    print(classroomUrl);
    final classroomResponse = await this.httpClient.get(Uri.parse(classroomUrl));
    print(classroomResponse.body);

    if (classroomResponse.statusCode != 200) {
      throw Exception('error getting students for class');
    }

    List<Parent> list = Parent.listFromJson(json.decode(classroomResponse.body));
    return list;
  }
}