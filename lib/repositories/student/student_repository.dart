import 'dart:async';

import 'package:meta/meta.dart';

import 'student_api_client.dart';
import 'package:attendanceapp/models/student.dart';
import 'package:attendanceapp/models/parent.dart';
import 'package:attendanceapp/models/attendance_info.dart';
class VisitorRepository {
  final StudentApiClient visitorApiClient;

  VisitorRepository({@required this.visitorApiClient})
      : assert(visitorApiClient != null);

  Future<List<Student>> getStudentByClass(int classId) async {
    return visitorApiClient.getStudentsByClass(classId);
  }
  Future<List<Student>> getTodayClassroomAttendance(int classroomId) async {
    return visitorApiClient.getTodayClassroomAttendance(classroomId);
  }
  Future<List<Parent>> getStudentParent(int studentId) async {
    return visitorApiClient.getStudentParent(studentId);
  }
}