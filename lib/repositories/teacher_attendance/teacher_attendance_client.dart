import 'package:attendanceapp/models/state.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'package:attendanceapp/models/attendance_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TeacherAttendanceClient {
  static const baseUrl = 'https://dashboard.anak2u.com.my';
  final http.Client httpClient;

  TeacherAttendanceClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<AttendanceInfo> getTeacherAttendanceInfo(int teacherId) async {
    final teacherUrl = '$baseUrl/api/v2/teacher-today-attendance/$teacherId';
    print(teacherUrl);
    final attendanceResponse = await this.httpClient.get(Uri.parse(teacherUrl));
    print(attendanceResponse.statusCode);
    if (attendanceResponse.statusCode != 200) {
      throw Exception('error getting teacher for class');
    }
    print(attendanceResponse.body);
    if (json.decode(attendanceResponse.body)["data"] != null) {
      AttendanceInfo info = AttendanceInfo.fromJson(
          json.decode(attendanceResponse.body));
      print(info);
      return info;
    }
    return new AttendanceInfo();
  }

  Future<StateModel> createNewAttendance(String teacherId, String checkInTime, String temperature,String date, File photoCheckIn, String notes) async {
    final newAttendanceUrl = "$baseUrl/api/v2/teacher-today-attendance";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');
    String base64Image = "";


    if (photoCheckIn != null) {
      List<int> imageBytes = await photoCheckIn.readAsBytes();
      base64Image = base64Encode(imageBytes);
      print(base64Image);
    }

    final response = await httpClient.post(
      Uri.parse(newAttendanceUrl),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
      body: {
        'teacher_id':teacherId,
        'check_in_time': checkInTime,
        'temperature':temperature,
        'date':date,
        'notes':notes,
        'photo_check_in':base64Image
      },
    );

    if(response.statusCode >= 500) return StateModel<String>.error("An error happens");
    print(response.body);
    final userJson = jsonDecode(response.body);

    if(response.statusCode < 200 || response.statusCode >= 300){
      if(userJson['data'] != null)
        return StateModel<String>.error('${userJson['data']}');
      else
        return StateModel<String>.error("An error happens");
    }


    AttendanceInfo userData = AttendanceInfo.fromJson(userJson);
    return StateModel<AttendanceInfo>.success(userData);

  }
  Future<StateModel> sendAttendanceInfo(int attendanceId, String checkOutTime, File photoCheckOut) async{
    final updateProfileUrl = "$baseUrl/api/v2/teacher-today-attendance/$attendanceId";
    String base64Image = "";
    if (photoCheckOut != null) {
      List<int> imageBytes = await photoCheckOut.readAsBytes();
      base64Image = base64Encode(imageBytes);
      print(base64Image);
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token'); final response = await httpClient.put(
      Uri.parse(updateProfileUrl),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
      body: {

        'check_out_time':checkOutTime,
        'photo_check_out':base64Image
      },
    );
    print(response.statusCode);
    if(response.statusCode >= 500) return StateModel<String>.error("An error happens");
    print(response.body);
    final userJson = jsonDecode(response.body);

    if(response.statusCode < 200 || response.statusCode >= 300){
      if(userJson['data'] != null)
        return StateModel<String>.error('${userJson['data']}');
      else
        return StateModel<String>.error("An error happens");
    }

    AttendanceInfo userData = AttendanceInfo.fromJson(userJson);
    return StateModel<AttendanceInfo>.success(userData);
  }



}