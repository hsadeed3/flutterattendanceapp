import 'dart:async';
import 'dart:io';

import 'package:meta/meta.dart';
import 'package:attendanceapp/models/state.dart';
import 'teacher_attendance_client.dart';
import 'package:attendanceapp/models/attendance_info.dart';

class TeacherAttendanceRepository {
  final TeacherAttendanceClient teacherAttendanceClient;

  TeacherAttendanceRepository({@required this.teacherAttendanceClient})
      : assert(teacherAttendanceClient != null);

  Future<AttendanceInfo> getTeacherAttendanceInfo(int teacherId) async {
    return teacherAttendanceClient.getTeacherAttendanceInfo(teacherId);
  }
  Future<StateModel> sendAttendanceInfo(int attendanceId, String checkOutTime, File photoCheckOut){
    return teacherAttendanceClient.sendAttendanceInfo(attendanceId,checkOutTime, photoCheckOut);
  }
  Future<StateModel> createNewAttendance(String teacherId, String checkInTime, String temperature,String date, File photoCheckIn, String notes){
    return teacherAttendanceClient.createNewAttendance(teacherId, checkInTime, temperature,date,photoCheckIn,notes);
  }
}