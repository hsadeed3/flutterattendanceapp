import 'package:attendanceapp/models/attendance_info.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:attendanceapp/models/classroom.dart';

class ClassroomApiClient {
  static const baseUrl = 'https://dashboard.anak2u.com.my';
  final http.Client httpClient;

  ClassroomApiClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Classroom>> getClassesBySchool(int schoolId) async {
    final classroomUrl = '$baseUrl/api/v2/classes/$schoolId';
    print(classroomUrl);
    final classroomResponse = await this.httpClient.get(Uri.parse(classroomUrl));
    print(classroomResponse.body);

    if (classroomResponse.statusCode != 200) {
      throw Exception('error getting students for class');
    }

    List<Classroom> list = Classroom.listFromJson(json.decode(classroomResponse.body));
    return list;
  }
}