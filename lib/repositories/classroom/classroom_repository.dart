import 'dart:async';

import 'package:attendanceapp/models/attendance_info.dart';
import 'package:attendanceapp/models/classroom.dart';
import 'package:meta/meta.dart';

import 'classroom_api_client.dart';


class ClassroomRepository {
  final ClassroomApiClient classroomApiClient;

  ClassroomRepository({@required this.classroomApiClient})
      : assert(classroomApiClient != null);

  Future<List<Classroom>> getStudentByClass(int schoolId) async {
    return classroomApiClient.getClassesBySchool(schoolId);
  }

}