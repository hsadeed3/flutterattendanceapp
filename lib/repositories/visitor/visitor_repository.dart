import 'dart:io';
import 'package:flutter/material.dart';
import 'visitor_api_client.dart';
import 'package:attendanceapp/models/state.dart';
class VisitorRepository {
  final VisitorApiClient visitorApiClient;

  VisitorRepository({@required this.visitorApiClient})
      : assert(visitorApiClient != null);

  Future<StateModel> registerVisitor(String name, String address, String contact, String purpose, String temperature) async =>
      await visitorApiClient.registerVisitor(name, address, contact,  purpose,  temperature);
}