import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:attendanceapp/models/state.dart';
import 'package:attendanceapp/models/visitor.dart';
import 'package:shared_preferences/shared_preferences.dart';
class VisitorApiClient {
  final http.Client httpClient;
  static const baseUrl = 'https://dashboard.anak2u.com.my/api/v2';

  VisitorApiClient({@required this.httpClient}) : assert(httpClient != null);
  Future<StateModel> registerVisitor(String name, String address, String contact, String purpose, String temperature) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');


    // if(await checkConnection()){
    //To change to adminLogin
    final registerVisitorUrl = '$baseUrl/visitor';
    final response = await this.httpClient.post(
      Uri.parse(registerVisitorUrl),
      headers: {
    HttpHeaders.authorizationHeader: 'Bearer $token',
    },
      body: {
        'name': name,
        'address': address,
        'contact':contact,
        'purpose':purpose,
        'temperature':temperature
      },
    );

    if(response.statusCode >= 500) return StateModel<String>.error("Sorry to  disappoint you, but something went wrong.");

    final responseBody = jsonDecode(response.body);
    print(responseBody);
    if (responseBody["response"] == "error") {
      return StateModel<String>.error('${responseBody["message"]}');
    }

    Visitor visitor = Visitor.fromJson(responseBody["data"]);

    return StateModel<Visitor>.success(visitor);
//    }else{
//      return StateModel<String>.error(errorInternetMsg);
//    }
  }
}