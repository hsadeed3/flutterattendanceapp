import 'dart:async';
import 'dart:io';
import 'package:meta/meta.dart';
import 'package:attendanceapp/models/state.dart';
import 'student_attendance_client.dart';
import 'package:attendanceapp/models/attendance_info.dart';

class StudentAttendanceRepository {
  final StudentAttendanceClient studentAttendanceClient;

  StudentAttendanceRepository({@required this.studentAttendanceClient})
      : assert(studentAttendanceClient != null);

  Future<AttendanceInfo> getStudentAttendanceInfo(int studentId) async {
    return studentAttendanceClient.getStudentAttendanceInfo(studentId);
  }
  Future<StateModel> sendAttendanceInfo(int attendanceId, String checkOutTime, File checkOutPic, String parentId, String temperature){
    return studentAttendanceClient.sendAttendanceInfo(attendanceId,checkOutTime, checkOutPic, parentId,temperature);
  }
  Future<StateModel> createNewAttendance(int studentId, String checkInTime, String checkOutTime, String temperature,String date, File checkInPhoto, String notes, String senderId,String temperatureout){
    return studentAttendanceClient.createNewAttendance(studentId, checkInTime, temperature,date, checkInPhoto,notes,  senderId);
  }


}