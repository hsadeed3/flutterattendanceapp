import 'package:attendanceapp/models/state.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'package:attendanceapp/models/attendance_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StudentAttendanceClient {
  static const baseUrl = 'https://dashboard.anak2u.com.my';
  final http.Client httpClient;

  StudentAttendanceClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<AttendanceInfo> getStudentAttendanceInfo(int studentId) async {
    final studentUrl = '$baseUrl/api/v2/today-attendance/$studentId';
    print(studentUrl);
    final attendanceResponse = await this.httpClient.get(Uri.parse(studentUrl));
    print(attendanceResponse.statusCode);
    if (attendanceResponse.statusCode != 200) {
      throw Exception('error getting students for class');
    }
    print(attendanceResponse.body);
if (json.decode(attendanceResponse.body)["data"] != null) {
  AttendanceInfo info = AttendanceInfo.fromJson(
      json.decode(attendanceResponse.body));
  return info;
}
return new AttendanceInfo();
  }

  Future<StateModel> createNewAttendance(int studentId, String checkInTime, String temperature,String date, File checkInPhoto, String notes, String senderId) async {
    final newAttendanceUrl = "$baseUrl/api/v2/today-attendance";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');
    String base64Image = "";
    if (checkInPhoto != null) {
      List<int> imageBytes = await checkInPhoto.readAsBytes();
      base64Image = base64Encode(imageBytes);
      print(base64Image);
    }
    final response = await httpClient.post(
      Uri.parse(newAttendanceUrl),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
      body: {
       'student_id':'$studentId',
        'check_in_time': checkInTime,
       'temperature':temperature,
        'date':date,
        'sender':senderId,
        'notes':notes,
        'photo_check_in':base64Image
      },
    );

    if(response.statusCode >= 500) return StateModel<String>.error("An error happens");
    final userJson = jsonDecode(response.body);
    print('here');
    if(response.statusCode < 200 || response.statusCode >= 300){
      if(userJson['data'] != null)
        return StateModel<String>.error('${userJson['data']}');
      else
        return StateModel<String>.error("An error happens");
    }


      AttendanceInfo userData = AttendanceInfo.fromJson(userJson);
      return StateModel<AttendanceInfo>.success(userData);

  }
  Future<StateModel> sendAttendanceInfo(int attendanceId,  String checkOutTime, File checkOutPic, String parentId, String temperature) async{

    final updateProfileUrl = "$baseUrl/api/v2/today-attendance/$attendanceId";
    print(updateProfileUrl);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String base64Image = "";
    if (checkOutPic != null) {
      List<int> imageBytes = await checkOutPic.readAsBytes();
      base64Image = base64Encode(imageBytes);

    }
    print(base64Image);
    var token = prefs.getString('token');
    if (base64Image == null){
      base64Image = "";
    }
    print(temperature);
    print(checkOutTime);
    print(base64Image);
    print(parentId);
    final response = await httpClient.put(
      Uri.parse(updateProfileUrl),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
      body: {
        'temperature_check_out':temperature,
        'check_out_time':checkOutTime,
        'photo_check_out':base64Image,
        'sender_check_out':parentId
      },
    );
    print(response.statusCode);
    if(response.statusCode >= 500) return StateModel<String>.error("An error happens");
    print(response.body);
    final userJson = jsonDecode(response.body);

    if(response.statusCode < 200 || response.statusCode >= 300){
      if(userJson['data'] != null)
        return StateModel<String>.error('${userJson['data']}');
      else
        return StateModel<String>.error("An error happens");
    }

    AttendanceInfo userData = AttendanceInfo.fromJson(userJson);
    return StateModel<AttendanceInfo>.success(userData);
  }

}