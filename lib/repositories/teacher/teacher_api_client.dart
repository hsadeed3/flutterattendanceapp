import 'package:attendanceapp/models/teacher.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:attendanceapp/models/student.dart';

class TeacherApiClient {
  static const baseUrl = 'https://dashboard.anak2u.com.my';
  final http.Client httpClient;

  TeacherApiClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Teacher>> getTeachersByInstitute(int instituteId) async {
    final teacherUrl = '$baseUrl/api/v2/teacher-today-attendance-institute/$instituteId';
    final teacherResponse = await this.httpClient.get(Uri.parse(teacherUrl));
    print(teacherResponse.body);
    if (teacherResponse.statusCode != 200) {
      throw Exception('error getting teachers for class');
    }
print(teacherResponse.body);
    List<Teacher> list = Teacher.listFromJson(json.decode(teacherResponse.body));
    return list;
  }
}