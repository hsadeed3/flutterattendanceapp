import 'dart:async';

import 'package:attendanceapp/models/teacher.dart';
import 'package:attendanceapp/repositories/teacher/teacher_api_client.dart';
import 'package:meta/meta.dart';

class TeacherRepository {
  final TeacherApiClient teacherApiClient;

  TeacherRepository({@required this.teacherApiClient})
      : assert(teacherApiClient != null);

  Future<List<Teacher>> getTeachersByInstitute(int instituteId) async {
    return teacherApiClient.getTeachersByInstitute(instituteId);
  }
}