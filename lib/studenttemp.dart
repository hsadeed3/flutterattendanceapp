import 'dart:developer';

import 'package:attendanceapp/blocs/student/blocs.dart';
import 'package:attendanceapp/fetchingdetail.dart';
import 'package:attendanceapp/repositories/student/repositories.dart';
import 'package:attendanceapp/utils/blecontroller.dart';
import 'package:attendanceapp/widgets/student_attendance/studentdetail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'RegistertThermometer.dart';
import 'models/student.dart';

class StudentTemperature extends StatefulWidget {

  final Student student;
  StudentTemperature({this.student});

  @override
  _StudentTemperatureState createState() => _StudentTemperatureState();
}

class _StudentTemperatureState extends State<StudentTemperature> {




  final BleController bleCtrl = Get.put(BleController(""));
  double _selectedTemp = 0;
  double _selectedTempPoint = 0;
  List<String> _findings = [];
  @override
  Widget build(BuildContext context) {
    print(_selectedTemp);
    return Scaffold(
        appBar: new AppBar(
            backgroundColor: Colors.amber[600],
            title: Text('Record Temperature')),
        body: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: EdgeInsets.fromLTRB(200, 0, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 50),
                      createTemperatureBox(35, Colors.yellow[400]),
                      createTemperatureBox(36, Colors.yellow),
                      createTemperatureBox(37, Colors.yellow[600]),
                      createTemperatureBox(38, Colors.yellow[700]),
                      createTemperatureBox(39, Colors.yellow[800]),
                      createTemperatureBox(40, Colors.yellow[900]),
                      createTemperatureBox(41, Colors.deepOrange[700])
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 50),
                      createTemperaturePointBox(.0, Colors.amber[50]),
                      createTemperaturePointBox(.1, Colors.amber[100]),
                      createTemperaturePointBox(.2, Colors.amber[200]),
                      createTemperaturePointBox(.3, Colors.amber[300]),
                      createTemperaturePointBox(.4, Colors.amber[400]),
                      createTemperaturePointBox(.5, Colors.amber),
                      createTemperaturePointBox(.6, Colors.amber[600]),
                      createTemperaturePointBox(.7, Colors.amber[700]),
                      createTemperaturePointBox(.8, Colors.amber[800]),
                      createTemperaturePointBox(.9, Colors.amber[900])
                    ],
                  ),

                Column(children: <Widget>[

                  new Image.asset(
                    'assets/logos/thermoMeter.jpg',

                    width: 250.0,
                    height: 250.0,
                    fit: BoxFit.cover,
                  ),
                    Obx(() => Text('STATUS:${bleCtrl.status}',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color:Colors.red))),
                    SizedBox(height: 20),
                    Text('🌡️',style: TextStyle(fontSize: 80),),
                    Obx(() => Text('TEMPERATURE: ${bleCtrl.temperature}',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.red),)),
                    SizedBox(height: 50),
                    Obx(() => Container(
                        height:50,
                        width:150,

                        child: bleCtrl.status != 'CONNECTED!'
                            ? ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                onPrimary: Colors.black87,
                                primary: Colors.orange[300],
                                minimumSize: Size(100, 50),
                                padding: EdgeInsets.symmetric(horizontal: 16),
                            ),
                                onPressed: bleCtrl.connect,

                                child: Text('SCAN',
                                    style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)))
                            : null)),
                 SizedBox(height: 15.0),
               ElevatedButton(

                 style: ElevatedButton.styleFrom(
                   onPrimary: Colors.black87,
                   primary: Colors.orange[300],
                   minimumSize: Size(100, 50),
                   padding: EdgeInsets.symmetric(horizontal: 18),
                 ),
                    onPressed: () => {

                   log( prefs.getString('string') ?? ''),

                      setState(() {
                      bleCtrl.temperature=' '.obs;
                    }),
                    },
                   child: Text('REFRESH SCAN',
                       style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold))
                  ),

                  ]),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 10),
                        Text('Illness', style: TextStyle(fontSize: 25)),
                        SizedBox(height: 10),
                        Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                createIllnessBox(
                                    "STOMACH ACHE", "stomachache.png"),
                                SizedBox(width: 10),
                                createIllnessBox(
                                    "SLIGHT FEVER", "slightfever.png"),
                                SizedBox(width: 10),
                                createIllnessBox("RED EYES", "redeyes.png"),
                              ],
                            ),
                            SizedBox(height: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                createIllnessBox(
                                    "CHICKEN POX", "chickenpox.png"),
                                SizedBox(width: 10),
                                createIllnessBox("HIGH FEVER", "highfever.png"),
                                SizedBox(width: 10),
                                createIllnessBox("FLU", "flu.png"),
                              ],
                            ),
                            SizedBox(height: 20),
                            Text('Other Findings',
                                style: TextStyle(fontSize: 25)),
                            SizedBox(height: 10),
                            Container(
                              width: 460,
                              color: Colors.grey[350],
                              child: TextField(
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'OpenSans',
                                  ),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.all(14.0))),
                            ),
                            SizedBox(height: 15),
                            RaisedButton(
                              padding: EdgeInsets.all(8.0),
                              color: Colors.lightBlueAccent,
                              child: Text('SUBMIT',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 25)),
                              onPressed: () async {
                                if (_selectedTemp == 0) {
                                  Fluttertoast.showToast(
                                      msg:
                                          "Please select temperature to proceed",
                                      toastLength: Toast.LENGTH_SHORT,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 16.0);
                                } else {
                                  final VisitorRepository studentRepository =
                                      VisitorRepository(
                                    visitorApiClient: StudentApiClient(
                                      httpClient: http.Client(),
                                    ),
                                  );
                                  Navigator.push(
                                      (context),
                                      MaterialPageRoute(
                                          builder: (context) => BlocProvider(
                                                create: (context) =>
                                                    StudentBloc(
                                                        studentRepository:
                                                            studentRepository),
                                                child: FetchingDetail(
                                                    student: widget.student),
                                              )));

                                  studentData['temp'] =
                                      "${_selectedTemp.toString().split(".")[0]}.${_selectedTempPoint.toString().split(".")[1]}";
                                  studentData['findings'] = _findings;
                                  /*setState(() {
                                    _selectedParent = result['parent'];
                                  });*/
                                  /*Navigator.of(context).pop({
                                    'temp':
                                        "${_selectedTemp.toString().split(".")[0]}.${_selectedTempPoint.toString().split(".")[1]}",
                                    'findings': _findings
                                  });*/
                                }
                              },
                            )
                          ],
                        )
                      ])
                ],
              ),
            ),
          ),
        ));
  }

  createIllnessBox(String name, String fileName) {
    return GestureDetector(
      onTap: () => {
        setState(() {
          if (!_findings.contains(name)) {
            _findings.add(name);
          } else {
            _findings.remove(name);
          }
        })
      },
      child: Container(
        child: Column(
          children: <Widget>[
            Image.asset(
              'assets/logos/$fileName',
              height: 100,
            ),
            SizedBox(height: 10),
            Text(name,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold))
          ],
        ),
        padding: EdgeInsets.all(20),
        decoration: _findings.contains(name)
            ? BoxDecoration(
                color: Colors.grey[350],
                border: Border.all(width: 1, color: Colors.deepOrange),
              )
            : BoxDecoration(
                color: Colors.grey[350],
              ),
      ),
    );
  }

  createTemperatureBox(double temp, Color color) {
    return GestureDetector(
        onTap: () => {
              setState(() {
                _selectedTemp = temp;
              })
            },
        child: Container(
            decoration: _selectedTemp == temp
                ? BoxDecoration(
                    color: color,
                    border: Border.all(width: 1, color: Colors.deepOrange),
                  )
                : BoxDecoration(color: color),
            child: Text('$temp ',
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            padding: EdgeInsets.all(25.0)));
  }

  createTemperaturePointBox(double temppoint, Color color) {
    return GestureDetector(
        onTap: () => {
              setState(() {
                _selectedTempPoint = temppoint;
              })
            },
        child: Container(
            decoration: _selectedTempPoint == temppoint
                ? BoxDecoration(
                    color: color,
                    border: Border.all(width: 1, color: Colors.deepOrange),
                  )
                : BoxDecoration(color: color),
            child: Text('$temppoint °C',
                style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            padding: EdgeInsets.all(14.0)));
  }
}
