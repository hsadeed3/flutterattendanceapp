import 'package:attendanceapp/utils/blecontroller.dart';
import 'package:attendanceapp/widgets/select_user.dart';
import 'package:flutter/material.dart';
import 'package:attendanceapp/widgets/login/login_screen.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:attendanceapp/utils/blecontroller.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}
void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
//    AutoOrientation.landscapeLeftMode();

    return MaterialApp(
      title: 'Anak2U Attendance Application',

      home:  LoginScreen(),

    );
  }

}
